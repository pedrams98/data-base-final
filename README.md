# SPOTIFY DEMO
## Project Overview

This project is aimed to present a music streaming web application.
## Team Members

Tina Tahvildari @TinaT   
Pedram Soofi @pedrams98   
Mohammad Sulaiman Behzad @sulaimanbehzad   

## Project Details
Developement frameworks & Tools used:
- Php 
- Vue.js
- Node.js
- MySQL
- Javascript
- JSON
- Multer(for images storage)


Tools used:  
1. XAMPP
2. PHPMyAdmin
3. Visual Studio Code 
4. Postman

##  Importanat directories
Database schema, ER diagram can be found under
- ##### ~DB\data-base-final\dataBase schema
Database scheme.sql file (for database setup can be imported in DBMS tool) and insert.sql file can be found  
- ##### ~DB\data-base-final\server\DB

## How to run the project
From termianl go in to the directories mentioned below the run the qouted codes:  
  
##### ~\DB\data-base-final\server
### Running server
```
node .\index.js
```
##### ~\DB\data-base-final\client\role
### Running client
```
npm run serve
```

