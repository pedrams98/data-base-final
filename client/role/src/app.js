import showMessage from './resources/components/showMessage.vue';
import {mapState } from 'vuex';
export default {
    components:{
        showMessage
	},
    computed: {
		...mapState([
			'message' , 
			'messageType',
		])
	},
	mounted() {
			
	},
}
