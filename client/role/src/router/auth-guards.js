import Vue from 'vue'
import VueCookies from 'vue-cookies'

export const AuthGuards = {
  router: null,
  setInstanceRouter: function (router) {
    this.router = router
  },

  registerAuthGuard: function () {

    this.router.beforeEach((to, from, next) => {    
      if(VueCookies.get("token") == null){
          if(to.name == 'login' || to.name == 'listenersignup' || to.name == 'artistsignup'){
              next()
          }
          else{
            next({name: "login"})  
          }
        
      }
      else {
        if(to.meta.rule && to.meta.rule !== VueCookies.get("rule"))
          next({name : "404page"})
        else 
          next()       
      }
    })
  }
}
