import { AccountRoutes } from '../features/account/routes';
import { ListenerRoutes} from '../features/listener/routes';
import { ArtistRoutes} from '../features/artist/routes';


export const routes = [
  // {
  //   path: '/',
  //   redirect: '/home'
  // },
  // {
  //   path: '/home',
  //   name: 'home',
  //   // component: () => import(/* webpackChunkName: "dashboard" */ '../features/dashboard/dashboard.vue')
  // },
  {
    path: '/account',
    children: AccountRoutes,
    name: 'account',
    component: () => import(/* webpackChunkName: "account" */ '../features/account/account.vue')
  },
  {
    path:'/artist',
    name: 'artist_dashboard',
    children: ArtistRoutes,
    meta : {
      rule : 'artist',
    },
    component: ()=> import('../features/artist/artist.vue')
  },
  {
    path:'/listener',
    name: 'listener_dashboard',
    children: ListenerRoutes,
    meta : {
      rule : 'listener',
    },
    component: ()=> import('../features/listener/listener.vue')
  },
  {
    path:'*',
    name: '404page',
    component: ()=> import('../resources/components/404.vue')
  }
]
