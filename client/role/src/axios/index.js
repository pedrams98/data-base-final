import axios from 'axios';
import interceptor from './interceptor';

// global axios defaults
axios.defaults.timeout = 30000;
// add interceptors
axios.interceptors.request.use(interceptor.request, interceptor.requestError);
axios.interceptors.response.use(interceptor.response, interceptor.responseError);


