import {APP} from '../constants'
export const mutations = {
  [APP.MESSAGE.SET_MESSAGE.MUTATION] : (state , message)=>{
    state[APP.MESSAGE.MESSAGE_TEXT] = message;
  },

  [APP.MESSAGE.SET_IS_SHOWING_MESSAGE.MUTATION] : (state , isShowing)=>{
    state[APP.MESSAGE.MESSAGE_IS_SHOWING] = isShowing;
  },

  [APP.MESSAGE.SET_MESSAGE_TYPE.MUTATION] : (state , type)=>{
    state[APP.MESSAGE.MESSAGE_TYPE] = type;
  }
}
