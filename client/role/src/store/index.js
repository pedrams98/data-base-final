import Vue from 'vue'
import Vuex from 'vuex'
import { state } from './state'
import { getters } from './getters'
import { actions } from './actions'
import { mutations } from './mutations'

Vue.use(Vuex)

const store = new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})

// if (module.hot) {
//   module.hot.accept([
//     './getters',
//     './actions',
//     './mutations',
//     '../features/employee/store/index',
//     '../features/account/store/index'
//   ], () => {
//     const newModuleA = require('../features/employee/store/index')
//     const newModuleB = require('../features/account/store/index');
//     store.hotUpdate({
//       getters: require('./getters'),
//       actions: require('./actions'),
//       mutations: require('./mutations'),
//       modules: {
//         employee: newModuleA,
//         account : newModuleB
//       }
//     })
//   })
// }

export default store
