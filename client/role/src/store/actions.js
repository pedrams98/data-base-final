import {APP} from '../constants'
export const actions = {

  [APP.MESSAGE.SET_MESSAGE.ACTION] : ({commit} , message)=>{
    commit(APP.MESSAGE.SET_MESSAGE.MUTATION , message);
  },

  [APP.MESSAGE.SET_IS_SHOWING_MESSAGE.ACTION] : ({commit} , isShowing)=>{
    commit(APP.MESSAGE.SET_IS_SHOWING_MESSAGE.MUTATION, isShowing);
  },
  [APP.MESSAGE.SET_MESSAGE_TYPE.ACTION] : ({commit} , type)=>{
    commit(APP.MESSAGE.SET_MESSAGE_TYPE.MUTATION , type);
  }
}