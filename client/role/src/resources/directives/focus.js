export default {
  bind: function (el, binding, vnode) {
    // console.log('vnode : ', vnode)
  },
  update: function (el, binding, vnode, oldVnode) {
  },
  // When the bound element is inserted into the DOM...
  inserted: function (el) {
    el.focus()
  }
}
