import Vue from 'vue';
import './axios/index';
import App from './app.vue';
// import './validation/index';
import router from './router';
import store from './store';
import './resources/index';
import { BootstrapVue, FormRadioPlugin } from 'bootstrap-vue'
import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';
import en from 'vee-validate/dist/locale/en.json';
import * as rules from 'vee-validate/dist/rules';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueCookies from 'vue-cookies'
import '../../assets/sass/style.scss';
import birthDatepicker from 'vue-birth-datepicker';
import vueCountryRegionSelect from 'vue-country-region-select'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import vueDebounce from 'vue-debounce'
Vue.use(vueCountryRegionSelect)

Vue.use(vueDebounce)
 
// Or if you want to pass in the lock option
Vue.use(vueDebounce, {
  lock: true
})
 
// Setting a different event to listen to
Vue.use(vueDebounce, {
  listenTo: 'input'
})
 
// Listening to multiple events
Vue.use(vueDebounce, {
  listenTo: ['input', 'keyup']
})

Vue.use(Loading , {
  loader: 'bars',
  color: "#0BBF7D",
  backgroundColor : "#111111",
});
Vue.use(VueToast, {
  // One of options
  position: 'top'
});

Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

localize('en', en);

Vue.use(BootstrapVue)
Vue.use(VueCookies)
Vue.use(birthDatepicker)
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Vue.$cookies.set('theme','default');
Vue.$cookies.set('hover-time','1s');
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
