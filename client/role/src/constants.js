const userVersion = ''
export const APP = {
  MESSAGE: {
     MESSAGE_TEXT : 'message',
     MESSAGE_IS_SHOWING : 'isShowMessage',
     MESSAGE_TYPE : 'messageType',
     SET_MESSAGE: {
      ACTION : "setMessage",
      MUTATION : "updateMessage"
    },
    SET_IS_SHOWING_MESSAGE : {
      ACTION : 'setIsShowingMessage',
      MUTATION : "updateIsShowing",
    },
    SET_MESSAGE_TYPE: {
      ACTION : "setMessageType",
      MUTATION : "updateMessageType"
    }
  }
}
