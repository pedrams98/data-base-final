export const ArtistRoutes = [
    {
        path: "/artist",
        redirect: 'dashboard'
    },
    {
        path: "dashboard",
        name: "artist-dashboard",
        component : ()=> import('./dashboard/dashboard.vue')
    },
]