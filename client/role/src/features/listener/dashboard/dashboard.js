  import { ValidationObserver, ValidationProvider } from "vee-validate";
import { extend } from 'vee-validate';
import { required, email, integer , confirmed , min , length} from 'vee-validate/dist/rules';
import test from '../test.json'
import search from '../search.json'
// import Track from '.././../../resources/components/track.vue'
import myTrack from '../../../resources/components/myTrack.vue';
import user from '../../../resources/components/user.vue';
import album from '../../../resources/components/album-card.vue'
import myPlaylist from '../../../resources/components/playlist-card.vue'
import { mapGetters } from 'vuex'
import { services } from "../service";
import axios from "axios"
export default {
  components: {
    ValidationObserver,
    ValidationProvider,
    myTrack,
    user,
    album,
    myPlaylist,
    },
    data(){
      return{
        data: test.data,
        search: search.data,
        user_type: "R",
        username: "perdam",
        playlistName : '',
        newPlaylistName :'',
        editedPlaylistName: '',
        reportMessage: "",
        nameState: null,
        nameState2: null,
        nameState3:null,
        selectedPlaylist: null,
        selectedTrack: null,
        options: [],
        editedPlaylist: null,
        searchText: "",
        searchResult: [],
        selectedReportedTrackId : null,
        trends : [],
        selectedPlaylistToShare: '',

        shareWithID : null,
      }
  },
  computed: {
    ...mapGetters([
      'getPlaylists',
      "isPlaylistLiked"
    ]),
    friends(){
      let following = this.$store.state.listener.profileInfo.following;
      let res = []
      if(following){
              following.forEach(user=>{
                res.push({
                  value : user.user_id,
                  text: user.username
              
                })
            })      
            return res;
          }
      }
    
  },
  mounted(){  
  //   this.$toast.open({
  //     message: 'Something went wrong!',
  //     type: 'error',
  //     position: "bottom-left"
  //     // all other options may go here
  // });
    // this.data = this.$store.getters.getPlaylists 
    this.getTrends()  

    if(this.user_type == "R"){
      this.goPremiumModalBackground= true;
    }    
  },  
  methods:{
   logout(){
    this.$bvModal.msgBoxConfirm('Please confirm that you want to logout.', {
      size: 'sm',
      buttonSize: 'sm',
      okVariant: 'danger',
      okTitle: 'YES',
      cancelTitle: 'NO',
      footerClass: 'p-2',
      hideHeaderClose: false,
      centered: true
    })
      .then(value => {
        if(value){
          this.$cookies.remove("token");
          this.$cookies.remove("rule");
          this.$router.push('/account/login')
        }
      })

   },
   playEvent(event){     
      services.playTrack(event.track_id).then((res)=>{
        this.$store.dispatch("playTrack" , event);
        this.$toast.open({
          message: res.data.message,
          type: 'success',
        });
      }).catch((error)=>{
        this.$toast.open({
          message: error.response.data.message,
          type: 'error',
        });
        
      })     
     

   },
   checkPlaylistName() {
    const valid = this.$refs.form.checkValidity()
    this.nameState = valid
    return valid
  },
  checkEditedPlaylistName() {
    const valid = this.$refs.form2.checkValidity()
    this.nameState2 = valid
    return valid
  },
  checkReportMessage(){
    const valid = this.$refs.form3.checkValidity();
    this.nameState3 = valid
    return valid
  },
    resetModal() {
      this.newPlaylistName = ''
      this.editedPlaylistName = ''
      this.reportMessage = ''
      this.nameState = null
      this.nameState2 = null
      this.nameState3 = null

    },
    handleAddNewPlaylistOK(bvModalEvt) {
      // Prevent modal from closing
      bvModalEvt.preventDefault()
      // Trigger submit handler
      this.handleAddPlaylistSubmit()
    },
    handleEditPlaylistOK(bvModalEvt){
      bvModalEvt.preventDefault();
      this.handleEditPlaylistSubmit();
    },
    handleReportOk(bvModalEvt){
      bvModalEvt.preventDefault();
      this.handleReportSubmit();
    },
    handleAddPlaylistSubmit() {
      if (!this.checkPlaylistName()) {
        return
      }
      this.$nextTick(() => {
        this.$bvModal.hide('choose-playlist-name')
      })
      let loader =  this.$loading.show();
      services.addPlaylist({playlist_title : this.newPlaylistName}).then((res)=>{
        this.$toast.open({
          message: res.data.message,
          type: 'success',
        });
        loader.hide();
        this.$store.dispatch("fetchPlaylists").catch(()=>{
          this.$toast.open({
            message: "cant not fetch playlists",
            type: 'error',
        });
        })
      }).catch((error)=>{
        let message =error.response.data.message
        loader.hide()
        this.$toast.open({
          message: message,
          type: 'error',
      });
      })
      
    },
    handleEditPlaylistSubmit(){
      if (!this.checkEditedPlaylistName()) {
        return
      }
      this.$nextTick(() => {
        this.$bvModal.hide('choose-edit-name')
      })
      // console.log(this.editedPlaylist);
      let loader =  this.$loading.show();
      services.editPlaylist(
        {playlist_title : this.editedPlaylist.playlist_title,
         new_title: this.editedPlaylistName}).then((res)=>{
              if(res.data.status == "success"){
                this.$toast.open({
                  message: res.data.message,
                  type: 'success',
              });
              loader.hide();
              this.$store.dispatch("fetchPlaylists")
            }
         }).catch((error)=>{
          loader.hide();
          let message =error.response.data.message
          loader.hide()
          this.$toast.open({
            message: message,
            type: 'error',
        });
         })
      
    },
    handleReportSubmit(){
      if (!this.checkReportMessage()) {
        return
      }
      this.$nextTick(() => {
        this.$bvModal.hide('report-modal')
      });
      let loader = this.$loading.show()
      services.reportTrack({
        track_id : this.selectedReportedTrackId,
        message : this.reportMessage
      }).then((res)=>{
          loader.hide();
          this.$toast.open({
            message: res.data.message,
            type: 'success',
        });   
      }).catch((error)=>{
          loader.hide();
          if(error.response.data.message.code == 'ER_DUP_ENTRY'){
            this.$toast.open({
              message: "you allready report this track",
              type: 'error',
          }); 
          }
          else{
            this.$toast.open({
              message: "an error occurred during report track",
              type: 'error',
          }); 
          }
          
        
      })      

    },
    addtoPlaylist(e){
      let playlists = this.getPlaylists
      this.options = []
      playlists.forEach(item=>{
        if(!this.isPlaylistLiked(item.playlist_id))
            this.options.push(item.playlist_title)
      })
      this.selectedTrack = e
      this.$bvModal.show("choose-playlist-modal")       
            
    },
    handleAddToPlaylistOK(){
      if(this.selectedPlaylist == null){
        this.$toast.open({
          message: 'please choose an playlist',
          type: 'error',
          // all other options may go here
      });
      }
      else{
        let data = {    
          "track_id" : this.selectedTrack,
          "playlist_title" : this.selectedPlaylist
        }
        let loader =  this.$loading.show();
        services.addTracKToPlaylist(data).then((res)=>{
            if(res.data.status == "success"){
              this.$toast.open({
                message: res.data.message,
                type: 'success',
            });
            loader.hide();
            this.$store.dispatch("fetchPlaylists")
          }
          
        }).catch((error)=>{  
          loader.hide();
          this.$toast.open({
            message: "track allready is in selected playlist",
            type: 'error',
        });
        })
        this.selectedPlaylist = null
      } 
    },
    editPlaylist(playlist){
      this.$bvModal.show('choose-edit-name')
      this.editedPlaylist = playlist
      // services.deletePlaylist(playlist.playlist_id  )
      
    },
    deletePlaylist(playlist){
      this.$bvModal.msgBoxConfirm('Please confirm that you want to delete this playlist.', {
        size: 'sm',
        buttonSize: 'sm',
        okVariant: 'danger',
        okTitle: 'YES',
        cancelTitle: 'NO',
        footerClass: 'p-2',
        hideHeaderClose: false,
        centered: true
      })
        .then(value => {
          if(value){
            let loader =  this.$loading.show();
            services.deletePlaylist(playlist.playlist_id).then((res)=>{
              if(res.data.status == "success"){
                this.$toast.open({
                  message: res.data.message,
                  type: 'success',
              });
              loader.hide();
              this.$store.dispatch("fetchPlaylists");
              this.$store.dispatch("getLikedTracks");

            }
            }).catch(error=>{
              this.$toast.open({
                message: "an error occurred during deleting",
                type: 'error',
            });
            })
          }
        })
    
      
    },
    onSearch(){
      // console.log(this.searchText);
      if(this.searchText.length >0){
        let loader = this.$loading.show()
        services.search(this.searchText).then((res)=>{
          loader.hide()
          this.searchResult = res.data.data;
  
        }).catch((error)=>{
            loader.hide();
            this.$toast.open({
              message: "nothing found",
              type: 'error',
           });
        })
      }
   
      
    },
    report(e){
      this.$bvModal.show('report-modal');
      this.selectedReportedTrackId =e    
    },
    getTrends(){
      
      services.getTrends().then(axios.spread((...responses) => {
        let fivePopularTrackOfWeek = responses[6]
        this.trends.push({
          type : 'track',
          title: "Popular Track Of Week",
          data : fivePopularTrackOfWeek.data.data
        })
        console.log(">>>>>>>>" , fivePopularTrackOfWeek);
        

        let lastFollowingPlay = responses[1]
        this.trends.push({
          type : 'track',
          title: "last Track That Your Following Played",
          data : lastFollowingPlay.data.data
        })

        let albumOfArtistWithSameNatiomality = responses[0]        
        this.trends.push({
          type : 'album',
          title : "Album Of Artist With Same Nationality",
          data : albumOfArtistWithSameNatiomality.data.data
        })    

        let getFivePopularTracksOfFavGenre = responses[2]
        this.trends.push({
          type : 'track',
          title: "Five Popular Tracks Of Your Fav Genre",
          data : getFivePopularTracksOfFavGenre.data.data
        })

        let getFiveNewTracksOfFavGenre = responses[3]
        this.trends.push({
          type : 'track',
          title: "Five New Tracks Of Your Fav Genre",
          data : getFiveNewTracksOfFavGenre.data.data
        })

        let getArtistWithSameDomGenreWithfavoriteArtist = responses[4]
        this.trends.push({
          type : 'user',
          title: "Artist That Have Same Dominant Genre With Your favorite Artist",
          data : getArtistWithSameDomGenreWithfavoriteArtist.data.data
        })
        let getNewTrackOfArtist = responses[5]
        this.trends.push({
          type : 'track',
          title: "New Track Of Artist That You Follow",
          data : getNewTrackOfArtist.data.data
        })
      
        let myPlaylist =   this.$store.state.listener.playlists;
        myPlaylist.forEach(playlist=>{
          services.getTrackSuggestFromDomGenreOfPlaylist(playlist.playlist_id).then((res)=>{
            this.trends.push({
              type : 'track',
              title: `Track hat have same dominant genre with ${playlist.playlist_title} playlist`,
              data : res.data.data.tracks
            })
          })

        })
          
        // }))
        // use/access the results 
      })).catch(errors => {
        // react on errors.
      })
      
     

    },
    share(title){
      this.selectedPlaylistToShare = title;    
      this.friends;  
      this.$bvModal.show("share-playlist-modal");
    },
    handleOkSharePlaylist(){
      if(this.shareWithID == null){
        this.$toast.open({
          message: 'please choose an user',
          type: 'error',
          // all other options may go here
      });
      }
      else{
        let data = {
          user_idS : this.shareWithID,
          playlist_title : this.selectedPlaylistToShare
        }
        let loader = this.$loading.show();
        services.sharePlaylist(data).then((res)=>{
          loader.hide();
          this.$toast.open({
            message: res.data.message,
            type: 'success',
        });   
        }).catch((error)=>{
          loader.hide()
          if(error.response.data.message.code == "ER_DUP_ENTRY"){
            this.$toast.open({
              message: 'You alreay share this playlist wirh this user',
              type: 'error',
              // all other options may go here
            });
          }
          else{
            this.$toast.open({
              message: 'an error occurred during shreing',
              type: 'error',
              // all other options may go here
            });
          }
        //   this.$toast.open({
        //     message: 'please choose an user',
        //     type: 'error',
        //     // all other options may go here
        // });
          
        })        
      this.shareWithID = null
        
      }
    }

  }
}