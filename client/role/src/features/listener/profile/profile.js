import axios from 'axios'
import user from '../../../resources/components/user.vue'
import { services } from '../service';
import {mapGetters} from 'vuex'
export default{
    components:{
        user
    },
    computed:{
        ...mapGetters([
            'hasProfileImage'
        ]),
        userInfo(){
            return this.$store.state.listener.profileInfo;
        },
        followers(){
            return this.userInfo.followers
        },
        followings(){
            return this.userInfo.following
        },
        getBDate(){
            let date = new Date( this.userInfo.birth_date);
            var dd = String(date.getDate()).padStart(2, '0');
            var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = date.getFullYear();
            date = yyyy + '-' + mm + '-' + dd;
            return date
        }
    },
    data(){
        return{
            noProfileImage : false,
            favGenre : null,
            favArtist: null
        }
    },
    mounted() {
        services.getFavoriteArtist().then((res)=>{
            this.favArtist = res.data.data
            
        }).catch((error)=>{
            this.favArtist = null;
        })
        services.getFavoriteGenre().then((res)=>{
            this.favGenre = res.data.data;
        }).catch(()=>{
            this.favGenre = null;
        })
        
    },

}