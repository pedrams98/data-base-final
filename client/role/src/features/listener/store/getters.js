
export const getters = {
    getPlaylists(state){
        return state.playlists;
    },
    isTrackLiked : (state)=>(track_id) =>{
        let likedtracks = state.likedTrack
        let isLiked = false;
        likedtracks.forEach(track=>{            
            if(track.track_id == track_id)
                isLiked = true                
        })
        return isLiked
        
    },
    isPlaylistLiked : (state)=>(playlist_id) =>{
        let likedPlaylists = state.likedPlaylists
        let isLiked = false;
        likedPlaylists.forEach(playlist=>{            
            if(playlist.playlist_id == playlist_id)
                isLiked = true                
        })
        return isLiked
    },
    isUserFollowed: (state)=>(user_id) =>{ 
        let following = state.profileInfo.following 
        let isFollowing = false
        following.forEach(user=>{
            if(user.user_id == user_id)
                isFollowing = true
        })
        return isFollowing
    },
    getProfile(state){
        return state.profileInfo
    },
    hasProfileImage(state){
        return state.hasProfileImage
    },
    isUserPlaylist : (state)=> (id)=>{        
        let playlists = state.playlists;                
        let bool = false;
        playlists.forEach(playlist=>{
            if(playlist.playlist_id == id)
                bool = true;
        })
        return bool;
    }

}
