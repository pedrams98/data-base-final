
import test from '../test.json'
import { services } from '../service';
export const actions = {
    playTrack({commit} , params){
        commit("playTrack" , params);
    },
    async fetchPlaylists({commit}){
        commit("fetchPlaylists", await services.fetchPlayLists())
    },
    async deleteTrackFromPlaylist({commit} , params){        
        commit("deleteTrackFromPlaylist" , await services.deleteTrackOfPlaylist(params))
    },
    async fetchUserProfile({commit}){
        commit("fetchUserProfile" , await services.fetchUserProfile())
    },
    async getLikedTracks({commit}){
        commit("getLikedTracks" , await services.getLikedTrack())
    },
    async getLikedPlaylists({commit}){
        commit("getLikedPlaylists" , await services.getLikedPlaylist())
    },
    async isHaveProfileImage({commit} , params){        
        commit('isHaveProfileImage' , await services.isHaveProfileImage(params))
    }

};
