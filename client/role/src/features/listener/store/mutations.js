import Vue from 'vue';
import { services } from '../service';
export const mutations = {
    playTrack(state , params ){        
        state.playingTrack = params;
    },
    fetchPlaylists(state , params){                
        state.playlists = params.data.data
    },
    deleteTrackFromPlaylist(state , params){
        if(params.data.status == "success"){
            Vue.$toast.open({
                message: params.data.message,
                type: 'success',
                // all other options may go here
            });
        }
        else{
            Vue.$toast.open({
                message: params.data.message,
                type: 'error',
                // all other options may go here
            });
        }
        
    },
    fetchUserProfile(state , params){
        if(params.data.status == "success"){
            state.profileInfo = params.data.data
        }
        else{
            Vue.$toast.open({
                message: params.data.message,
                type: 'error',
                // all other options may go here
            });
        }
        
    },
    getLikedTracks(state , params){        
        if(params.data.status == "success"){
            state.likedTrack = params.data.data
        }
        else{
            Vue.$toast.open({
                message: params.data.message,
                type: 'error',
            });
        }
    },
    getLikedPlaylists(state , params){
        
        if(params.data.status == "success"){
            state.likedPlaylists = params.data.data
        }
        else{
            Vue.$toast.open({
                message: params.data.message,
                type: 'error',
            });
        }
    },
    isHaveProfileImage(state , params){
        if(params.data.message =="image not exist"){
           state.hasProfileImage = false
        }  
        else 
           state.hasProfileImage = true              
     
    }
   
}
