import axios from 'axios';
import Vue from 'vue'
let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Imxpc3RlbmVyRGVtbyIsInVzZXJfaWQiOjg0LCJpYXQiOjE1OTQ2MTIwNTcsImV4cCI6MTU5NDY1NTI1N30.jZHz5MKfGdWo7FGqzdr1TdnsAKqyntXgVTtO6GzMS70"
export const services = {
    fetchUserProfile(){                
        return axios.get("/myapi/account/profile" , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })
    },
    fetchPlayLists(){
        return axios.get("/myapi/listener/playlist" , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })
    },
    deleteTrackOfPlaylist(data){
        return axios.delete("/myapi/listener/deleteTrack" , {
            headers:{
                authorization: Vue.$cookies.get("token")
            },
            data:{
                track_id : data.track_id,
                playlist_title : data.playlist_title
            }
        })
    },
    addTracKToPlaylist(data){        
        return axios.post("/myapi/listener/addTrack" , data , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })
    },
    addPlaylist(data){
        return axios.post("/myapi/listener/addPlaylist" , data , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })  
    },
    deletePlaylist(id){
        return axios.delete("/myapi/listener/deletePlaylist",{
            params:{
                playlist_id : id
            },
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        }) 
    },
    editPlaylist(data){
         return axios.post("/myapi/listener/editPLaylist", {new_title : data.new_title}, {
            params:{
                playlist_title : data.playlist_title
            },
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        }) 
    },
    search(searchText){
        return axios.get('/myapi/search', {
            params:{
                q: searchText
            },
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })         
    },
    getLikedTrack(){
        return axios.get("/myapi/listener/getLikedTrack" , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })
    },
    likeTrack(id){
        let data = {
            track_id :id
        }
        return axios.post("/myapi/listener/likeTrack" ,data, {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })        
    },
    unlikeTrack(id){
        return axios.delete("/myapi/listener/unlikeTrack", {
            headers:{
                authorization: Vue.$cookies.get("token")
            },
            data: {
                track_id: id
            }
        })    
     },
    reportTrack(data){
        return axios.post('/myapi/listener/reportTrack', data , {
            headers:{
                authorization: Vue.$cookies.get("token")
            },
        })
    },
    follow(username){
        return axios.post("/myapi/follow" , {} , {
            headers:{
                authorization: Vue.$cookies.get("token")
            },
            params:{
                username : username
            }
        })
    },
    unfollow(username){
        return axios.put('/myapi/unfollow' , {} , {
            headers:{
                authorization: Vue.$cookies.get("token")
            },
            params:{
                username : username
            }
        })
    },
    getTrends(){
        let albumOfArtistWithSameNatiomality = axios.get("/myapi/listener/suggestArtistsAlbumsWithSameNationality", {headers:{authorization:Vue.$cookies.get("token")}});
        let lastFollowingPlay = axios.get("/myapi/listenre/lastFollowingPlay", {headers:{authorization:Vue.$cookies.get("token")}});
        let getFivePopularTracksOfFavGenre = axios.get('/myapi/listener/getFivePopularTracksOfFavGenre' , {headers:{authorization:Vue.$cookies.get("token")}});
        let getFiveNewTracksOfFavGenre = axios.get('/myapi/listener/getFiveNewTracksOfFavGenre' , {headers:{authorization:Vue.$cookies.get("token")}});
        let getArtistWithSameDomGenreWithfavoriteArtist = axios.get('/myapi/listener/getArtistWithSameDomGenreWithfavoriteArtist' , {headers:{authorization:Vue.$cookies.get("token")}});
        let getNewTrackOfArtist = axios.get('/myapi/listener/getNewTrackOfArtist' , {headers:{authorization:Vue.$cookies.get("token")}});
        let fivePopularTrackOfWeek = axios.get('/myapi/listener/fivePopularTrackOfWeek' , {headers:{authorization:Vue.$cookies.get("token")}});
        let all = [];

        all.push(albumOfArtistWithSameNatiomality , lastFollowingPlay ,
            getFivePopularTracksOfFavGenre,getFiveNewTracksOfFavGenre ,
            getArtistWithSameDomGenreWithfavoriteArtist , getNewTrackOfArtist  , fivePopularTrackOfWeek);
            
        return axios.all(all)
    },
    sharePlaylist(data){
        return axios.post("/myapi/listener/sharePlaylist" , data , {
            headers:{
                authorization: Vue.$cookies.get("token")
            },
        })
    },
    getLikedPlaylist(){
        return axios.get("/myapi/listener/likedPlaylist" , {
            headers:{
                authorization: Vue.$cookies.get("token")
            },
        })
    },
    likePlaylist(id){
        return axios.post('/myapi/listener/likePlaylist' , {"playlist_id": id} , {
            headers:{
                authorization: Vue.$cookies.get("token")
            },
        })
    },
    unlikePlaylist(id){
        return axios.delete('/myapi/listener/unlikePlaylist',{
            headers:{
                authorization: Vue.$cookies.get("token")
            },
            data : {"playlist_id": id}
        })
    },
    upgradeAccount(data){
        return axios.post("/myapi/listener/account/upgrade" , data , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })
    },
    playTrack(id){
        return axios.post("/myapi/listener/playMusic" , {track_id : id} , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })
    },
    getFavoriteGenre(){
        return axios.get("/myapi/listener/favoriteGenre" , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })
    },
    getFavoriteArtist(){
        return axios.get("/myapi/listener/favoriteArtistOfListener" , {
            headers:{
                authorization: Vue.$cookies.get("token")
            }
        })
    },
    isHaveProfileImage(username){
       return axios.get(`/myapi/profileimage?user=${username}`)                            
    },
    async getTrackSuggestFromDomGenreOfPlaylist(playlist_id){
        // let rq1 = axios.get("/myapi/suggestTwoTrackFromDominantGenreOfPlaylist", {data:{playlist_id : playlists[0].playlist_id}})
        // let rq2 = axios.get("/myapi/suggestTwoTrackFromDominantGenreOfPlaylist", {data:{playlist_id : playlists[0].playlist_id}})
        // let rq3 = axios.get("/myapi/suggestTwoTrackFromDominantGenreOfPlaylist", {data:{playlist_id : playlists[0].playlist_id}})
        // let rq4 = axios.get("/myapi/sugg
        return  await axios.get("/myapi/suggestTwoTrackFromDominantGenreOfPlaylist", {headers:{authorization: Vue.$cookies.get("token")},params:{playlist_id :playlist_id}}); 
    }
}