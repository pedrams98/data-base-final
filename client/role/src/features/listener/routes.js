export const ListenerRoutes = [
    {
        path: "/listener",
        redirect: 'dashboard'
    },
    {
        path: "dashboard",
        name: "listener-dashboard",
        component : ()=> import('./dashboard/dashboard.vue')
    },
    {
        path: "profile",
        name: "listener-profile",
        component : ()=> import('./profile/profile.vue')
    }
]