import navbar from '../../resources/components/navbar.vue'
import playarea from '../../resources/components/playArea.vue'
import { actions, getters, mutations, state } from './store/index'
import store from '../../store/index'
import { services } from './service'
import { ValidationObserver, ValidationProvider } from "vee-validate";
import { extend } from 'vee-validate';
import { required, email, integer , confirmed , min , length} from 'vee-validate/dist/rules';
import {mapGetters} from 'vuex'
extend('min', {
  validate(value) {
      return value.length > 8
  },
  message: "too Short! must be at least 8 character"
});

extend('positive', value => {
  return value >= 0;
});

extend("isMonth", value=>{
  return value < 13
})
extend("isYear", value=>{
  
  return value >= 20
})

extend('length' , {
  ...length,
  message : "invalid!!"
})
extend('required', {
  ...required,
  message: 'This field is required!'
});

extend("integer" , {
  ...integer,
  message: 'This field must be integer!'
})
extend('email', {
  ...email,
  message : 'This feild must be email!'
});
extend("confirmed" , {
  ...confirmed,
  message : "This field must match with password field!"
})
export default{
    data(){
        return{
          goPremiumModalBackground: false,
          goPremiumModalContent: false,
          updateAccountModal : false,
          cardNumber : [],
          cardExpr:[],
          month_count: null,

        }
    },
   components:{
        navbar,
        playarea,
        ValidationObserver,
        ValidationProvider,    
   },
   created () {
    store.registerModule('listener', { state, getters, actions, mutations })
  },
  
  beforeMount() {
    this.$store.dispatch("fetchPlaylists");
    this.$store.dispatch("getLikedTracks");
    this.$store.dispatch("getLikedPlaylists");
    this.$store.dispatch("fetchUserProfile").then(()=>{
      this.$store.dispatch('isHaveProfileImage' ,this.$store.state.listener.profileInfo.username)      
    });

  },
  mounted() {    
    if(this.getProfile.user_type == "R")
        this.goPremiumModalBackground = true;
  },
  beforeDestroy () {
    /* if you want to use account's state inside other modules you shouldn't use unregisterModule */
    // store.unregisterModule('account')
  },
  computed: {
    ...mapGetters([
      'getProfile'
    ])
  },
  methods:{
    goPro(){
      this.goPremiumModalBackground = true;
      
    },
    upgradeAccountSubmit(){
        let cardNumer =''
        this.cardNumber.forEach(item=>{
          cardNumer = cardNumer + item
        })
        let exprDate = "20" + this.cardExpr[0] + '-' +  this.cardExpr[1] + "-01"
        let data= {
          card_number : cardNumer,
          expiration_date : exprDate,
          month_count : this.month_count
        }
        let loader = this.$loading.show();
        services.upgradeAccount(data).then((res)=>{
            loader.hide();
            this.$toast.open({
              message: res.data.message,
              type: 'success',
           });
           this.updateAccountModal = false;
           this.goPremiumModalBackground = false;
           this.$store.dispatch("fetchUserProfile")
          
        }).catch((error)=>{
          this.$toast.open({
            message: error.response.data.message,
            type: 'error',
         });
          
        })
        

      
    
    }
  }
}