
export const AccountRoutes = [
  {
    path: '/account',
    redirect: 'profile'
  },
  {
    path: 'login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "login" */ './login/login.vue')
  },
  {
    path: "artistsignup",
    name: "artistsignup",

    component: ()=> import("./signup/ArtistSignup/artistsignup.vue")
  },
  {
    path:"listenersignup",
    name : "listenersignup",
    component: ()=> import("./signup/ListenerSignuup/listenerSignup.vue")
  }

]
// The async component factory can also return an object of the following format:
// '/* webpackChunkName: "{{name}}" */'.replace(/{{name}}/g, fileName) + filePath;
/*
const component = () => ({
  // The component to load (should be a Promise)
  component: import('./MyComponent.vue'),
  // A component to use while the async component is loading
  loading: LoadingComponent,
  // A component to use if the load fails
  error: ErrorComponent,
  // Delay before showing the loading component. Default: 200ms.
  delay: 200,
  // The error component will be displayed if a timeout is
  // provided and exceeded. Default: Infinity.
  timeout: 3000
}); */
