import router from "../../../router";
import { ValidationObserver, ValidationProvider } from "vee-validate";
import { extend } from 'vee-validate';
import { required, email, integer , confirmed , validate} from 'vee-validate/dist/rules';
import { mapActions } from 'vuex';
import service from "../service";

extend('required', {
    ...required,
    message: 'This field is required!'
});

extend("integer" , {
    ...integer,
    message: 'This field must be integer!'
})
extend('email', {
    ...email,
    message : 'This feild must be email!'
});
extend("confirmed" , {
    ...confirmed,
    message : "This field must match with password field!"
})

extend('okPassword', {
    validate(value) {
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/;
        return re.test(value);
    },
    message: "Password must contain Uppercase and Lowercase letter and digit!"
});

extend('min', {
    validate(value) {
        return value.length > 8
    },
    message: "too Short! must be at least 8 character"
});

export default {
    data: () => ({
        loading:false,
        showModal:false,
        quModal: false,
        changePassModal: false,
        loading: false,
        Q1:"",
        Q2:"",
        Q3:"",
        QuEmail:'',
        newPassword:'',
        confirmation: null,
        // username: "",
        // password: "",
        username: "",
        password: "",
        user_id : null,
        changePassToken : null,
    }),
    methods: {
        ...mapActions([
            'setIsShowingMessage',
            'setMessage',
            'setMessageType'
        ]),  
        onSubmit() {
            this.$refs['login-modal'].show();
        },
        handleLoginAsListener(){
            let loader = this.$loading.show()
            service.login({
                username : this.username , 
                password : this.password , 
                user_type : "R"
            }).then((response)=>{
                loader.hide()

                 this.$toast.open({
                message: response.data.message,
                    type: 'success',
                });
                this.$cookies.set("token" , response.data.token)
                this.$cookies.set("rule" , "listener")
                this.$store.dispatch("fetchUserProfile");
                router.push("/listener")
            }).catch((error)=>{
                loader.hide();
                this.$toast.open({
                    message: "sign in failed",
                    type: 'error',
                });
                
            })
        },
        handleLoginAsArtist(){
            let loader = this.$loading.show()
            service.login({
                username : this.username , 
                password : this.password , 
                user_type : 'A',
            }).then((response)=>{
                loader.hide()
                this.$toast.open({
                    message: response.data.message,
                        type: 'success',
                    });
                this.$cookies.set("token" , response.data.token)
                this.$cookies.set("rule" , "artist")
                router.push("/artist")
            }).catch((error)=>{
                loader.hide()
                this.$toast.open({
                    message: "sign in failed",
                    type: 'error',
                });
            })
        },
        handleSignup(){
            this.$refs['signup-modal'].show()
        },
        handleRecoverPassword(){
            this.showModal = true;
        },
        handleSignupAsListener(){
            router.push('./listenersignup')
        },
        handleSignupAsArtist(){
            router.push('./artistsignup')

        },
        handleAnswers(){
            let data={
                email : this.QuEmail,
                ans1 : this.Q1,
                ans2 : this.Q2,
                ans3 : this.Q3,                
            }
            let loader = this.$loading.show()
            service.verifyAnswres(data).then((response)=>{
                this.user_id = response.data.user_id;
                this.changePassToken = response.data.token;
                // this.$cookies.set('token'  , response.data.token)
                loader.hide()
                this.$toast.open({
                    message: response.data.message,
                        type: 'success',
                    });
                 this.quModal = false;
            }).catch((error)=>{
                loader.hide()
                this.$toast.open({
                    message: "verifying answres failed",
                    type: 'error',
                });
                
            })
        },
        handleNewPassword(){
            let loader = this.$loading.show()
            let data = {
                token : this.changePassToken,
                new_password : this.newPassword
            }       
            console.log(data); 
            service.changePassword(data).then((response)=>{

                loader.hide()
                this.$toast.open({
                    message: response.data.message,
                    type: 'success',
                });
                this.changePassModal = false;

            }).catch(()=>{
                loader.hide()
                this.$toast.open({
                    message: "change password failed",
                    type: 'error',
                });
                
            })
        }
    }
};