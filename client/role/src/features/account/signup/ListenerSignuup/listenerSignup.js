import { ValidationObserver, ValidationProvider } from "vee-validate";
import { extend } from 'vee-validate';
import { required, email, integer , confirmed} from 'vee-validate/dist/rules';
import router from '../../../../router'
import { mapActions } from 'vuex';
import birthDatepicker from 'vue-birth-datepicker/src/birth-datepicker';
import service from '../../service'

// import { signupFunc } from '../service'

extend('required', {
    ...required,
    message: 'This field is required!'
});

extend("integer" , {
    ...integer,
    message: 'This field must be integer!'
})
extend('email', {
    ...email,
    message : 'This feild must be email!'
});
extend("confirmed" , {
    ...confirmed,
    message : "This field must match with password field!"
})

extend('okPassword', {
    validate(value) {
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/;
        return re.test(value);
    },
    message: "Password must contain Uppercase and Lowercase letter and digit!"
});

extend('min', {
    validate(value) {
        return value.length > 8
    },
    message: "too Short! must be at least 8 character"
});


export default {
    components: {
        ValidationObserver,
        ValidationProvider,
        formItem : ()=> import('../../../../resources/components/form-item.vue'),
        birthDatepicker
    },
    data() {
        return {

            username: "",
            password: "",
            email:"",
            picSrc : "",
            test: "",
            isPicLoaded :false,
            firstName : "",
            lastName : '',
            country: '',
            date: "",
            Q1 : '',
            Q2 : '',
            Q3 : '',
            loading:false,
            confirmation: null,
            loading:false,
            // username: "listenertest3",
            // password: "psPS8693&",
            // email:"listenertest3@sadas.com",
            // picSrc : "",
            // test: "",
            isPicLoaded :false,
            // firstName : "listenertest",
            // lastName : 'listenertest',
            // country: '',
            // date: "",
            // Q1 : 'listenertest',
            // Q2 : 'listenertest',
            // Q3 : 'listenertest',
            profilePic :null,
            
        }
    },
    methods: {
        onSubmit(){
            let data = {
                username : this.username,
                email : this.email,
                password: this.password,
                firstname: this.firstName,
                lastname: this.lastName,
                nationality: this.country,
                birth_date: this.date,
                user_type: "R",
                ans1: this.Q1,
                ans2: this.Q2,
                ans3: this.Q3,
            }
            
            let loader = this.$loading.show()

            service.signup(data).then((response)=>{
                if(this.profilePic){
                    var formData = new FormData();
                    formData.append("image" , this.profilePic)
                    service.uploadProfileImage(this.username , formData);
                }
                this.$toast.open({
                    message: response.data.message,
                    type: 'success',
                });
                loader.hide();
                router.push('/account/login');
                
            }).catch((error)=>{
                loader.hide();
                this.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                });
            })
        },
        handleBack(){
            router.go(-1);
        },
        readURL($event) {
            this.isPicLoaded = true;            
            let input = $event.srcElement;
            if (input.files && input.files[0]) {
                this.profilePic = input.files[0] 
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById("pic-test").style.background = 'url("' + e.target.result + '"';
                    document.getElementById("pic-test").style.backgroundRepeat = "round"
                    document.getElementById("pic-icon").style.opacity = 0.8;
                };
                reader.readAsDataURL(input.files[0]);
               
            }
        }

    },

}