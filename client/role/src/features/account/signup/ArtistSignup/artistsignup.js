import { ValidationObserver, ValidationProvider, normalizeRules } from "vee-validate";
import { extend } from 'vee-validate';
import { required, email, integer , confirmed} from 'vee-validate/dist/rules';
import { mapActions } from 'vuex';
import router from "../../../../router";
import birthDatepicker from 'vue-birth-datepicker/src/birth-datepicker';
import service from '../../service'

// import { signupFunc } from '../service'


extend('required', {
    ...required,
    message: 'This field is required!'
});

extend("integer" , {
    ...integer,
    message: 'This field must be integer!'
})
extend('email', {
    ...email,
    message : 'This feild must be email!'
});
extend("confirmed" , {
    ...confirmed,
    message : "This field must match with password field!"
})

extend('okPassword', {
    validate(value) {
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/;
        return re.test(value);
    },
    message: "Password must contain Uppercase and Lowercase letter and digit!"
});

extend('min', {
    validate(value) {
        return value.length > 8
    },
    message: "too Short! must be at least 8 character"
});


export default {
    components: {
        ValidationObserver,
        ValidationProvider,
        birthDatepicker
    },
    data() {
        return {
            loading:false,
            confirmation : null,
            profilePic: null,
            artisticName : '',
            dateOFartistic: '',
            username: "",
            password: "",
            email:"",
            picSrc : "",
            test: "",
            isPicLoaded :false,
            firstName : "",
            lastName : '',
            country: '',
            bdate: "",
            Q1 : '',
            Q2 : '',
            Q3 : '',
           
            // username: "artistDemo3",
            // password: "artistDemo3",
            // email:"artistDemo3@gm.com",
            // picSrc : "",
            // isPicLoaded :false,
            // firstName : "artistDemo3",
            // lastName : 'artistDemo3',
            // country: '',
            // bdate: "",
            // Q1 : 'artistDemo3',
            // Q2 : 'artistDemo3',
            // Q3 : 'artistDemo3',
            artisticName : '',
            dateOFartistic: '',
            
        }
    },
    methods: {
        ...mapActions([
            'setIsShowingMessage',
            'setMessage',
            'setMessageType'
        ]),
        onSubmit(){
            let data = {
                username : this.username,
                email : this.email,
                password: this.password,
                firstname: this.firstName,
                lastname: this.lastName,
                nationality: this.country,
                birth_date: this.bdate,
                user_type: "A",
                ans1: this.Q1,
                ans2: this.Q2,
                ans3: this.Q3,
                artistic_name : this.artisticName,
                activity_start_data : this.dateOFartistic,
            }
            let loader = this.$loading.show()
            service.signup(data).then((response)=>{
                if(this.profilePic){
                    var formData = new FormData();
                    formData.append("image" , this.profilePic)
                    service.uploadProfileImage(this.username , formData);
                }
                this.$toast.open({
                    message: response.data.message,
                    type: 'success',
                });
                loader.hide();
                router.push('/account/login');
                
            }).catch((error)=>{                
                loader.hide();
                    this.$toast.open({
                        message: error.response.data.message,
                        type: 'error',
                    });
            })

        },
        handleBack(){
            router.go(-1);
        },
        readURL($event) {
            this.isPicLoaded = true;
            let input = $event.srcElement;
            if (input.files && input.files[0]) {
                this.profilePic = input.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById("pic-test").style.background = 'url("' + e.target.result + '"';
                    document.getElementById("pic-test").style.backgroundRepeat = "round"
                    // document.getElementById("pic-test").style.opacity = 0.5
                    document.getElementById("pic-icon").style.opacity = 0.8;
                    // $('#profile-pic')
                    //     .attr('src', e.target.result)
                    //     .width(150)
                    //     .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

    },

}