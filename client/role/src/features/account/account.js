import { actions, getters, mutations, state } from './store/index'
import store from '../../store/index'

export default {
  name: 'account',
  created () {
    // store.registerModule('account', { state, getters, actions, mutations })
  },
  mounted () {
  },
  beforeDestroy () {
    /* if you want to use account's state inside other modules you shouldn't use unregisterModule */
    // store.unregisterModule('account')
  }
}
