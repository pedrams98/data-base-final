import axios from 'axios'

export default {
  type: 'Life',
  login: function(params) {
   return axios({
      method: 'POST',
      url: '/myapi/account/login',
      data: params
    })
  },
  verifyAnswres: (params)=>{
    return axios({
      method: 'POST',
      url: '/myapi/account/verifySecurityAnswres',
      data: params
    })
  },
  changePassword : (params)=>{
    return axios({
      method: 'PUT',
      url: '/myapi/account/changePassword',
      headers : {
        authorization : params.token
      },
      data: {
        new_password : params.new_password
      }
    })
  },
  logout: function() {
  },
  signup: (data)=> {
    return axios({
      method: 'POST',
      url: '/myapi/account/signup',
      data: data
    })   
  },
  uploadProfileImage:(username , formData )=>{
    return axios.post("/myapi/profileimage"  , formData , {
      headers: {
        'Content-Type': 'multipart/form-data',
        name: username
      }
    })
    // return axios({
    //   method: 'POST',
    //   url : "/myapi/profileimage",
    //   headers:{
    //     name: username
    //   },
    //   data:{
    //     image: pic
    //   }
    // })
  }
};
