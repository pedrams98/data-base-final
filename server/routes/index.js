const express = require("express");
const router = express.Router();
const services = require('../DB/index');
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
const { response, request } = require("express");
const accessTokenSecret = 'databaseProjectFinalsecretPasskey';

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {            
        jwt.verify(authHeader, accessTokenSecret, (err, user) => {            
            if (err) {
                return res.status(403).send({
                    status : "error",
                    message: "authentication error!"
                });
            }            
            req.user = user;
            next();
        });
    } else {
        res.status(401).send({
            status : "error",
            message: "authentication error!"
        });
    }
};


const storage = multer.diskStorage({
    destination: (req , file , cb)=>{          
        cb(null , './uploads/')
    },
    filename : (req , file, cb)=>{  
        let type = file.mimetype.split('/')[1]
       cb(null , req.headers.name + '.jpg')    
    }
})

const filterFile = (req , file , cb)=>{    
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png'){
        cb(null , true)
    }
    else
        cb(null , false)
}

const upload = multer({ storage : storage,
fileFilter : filterFile
});


router.post('/profileimage'  , upload.single('image'), (request ,  response, next)=>{
   response.send("ok")
})

router.get('/profileimage' , (request , response)=>{
    let user = request.query.user;    
    let path_file = './uploads/' +  user + '.jpg';
    fs.exists(path_file , (exist)=>{        
        if(exist){
            response.sendFile(path.resolve(path_file));
        }
        else{
            response.status(200).send({
                message: 'image not exist'
            })
        }
    })
        
     
})

router.post('/account/signup' , (request , response)=>{ 
    
    services.signup(request.body).then(()=>{
        response.send({
            status : 'success',
            message : "you have signed up successfully!"
        })
    }).catch((err)=>{
        let message = null;
        if(err.code === 'ER_DUP_ENTRY')
            message = "username and email must be unique!!"
        response.status(400).send({
            status : 'error',
            message : message ? message : 'an error occurred during siging up!!'
        })
        
    })
})

router.post('/account/login' , (request , response)=>{

    services.login(request.body).then((id)=>{
        let token = jwt.sign({
            username: request.body.username,
            user_id : id
          }, accessTokenSecret, { expiresIn: '12h' }); 
        response.send({
            status : 'success',
            message: "you have sign in succsessfully!",
            token: token
        })
    }).catch((error)=>{
        response.status(400).send({
            status : 'error',
            message : error,
           
        })
    }) 
       
})

router.post("/account/verifySecurityAnswres" , (request , response)=>{
    services.verifySecurityAnswers(request.body).then((id)=>{
        let token = jwt.sign({
            id: id
          }, accessTokenSecret, { expiresIn: '10m' }); 
        response.send({
            status : "success",
            message : "user verified successfully!",
            user_id: id,
            token: token
        }) 
    }).catch((error)=>{
        response.status(400).send({
            status : "error",
            message : error
        })
    })
})

router.put('/account/changePassword' , authenticateJWT, (request , response)=>{
    let user_id = request.user.id
    let data = {
        ...request.body,
        user_id
    }
    services.changePassword(data).then(()=>{
        response.send({
            status : "success",
            message : "password changed successfully!!"
        })

    }).catch((error)=>{
        response.status(400).send({
            status : "error",
            message : error
        })
    }) 
})

router.get("/account/profile", authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    // if want other user data user_id come from query 
    if(request.query.id)
        user_id = request.query.id;
            
    services.getProfile(user_id).then((data)=>{
        response.send({
            status : "success",
            message: "get user profile information successfully",
            data: data
        })
        
    }).catch((error)=>{
        response.status(400).send({
            status : "error",
            message : error
        })
        
    })
})


router.post("/follow" , authenticateJWT , (request , response)=>{
    let user1 = request.user.username;
    let user2 = request.query.username;
    if(user1 != user2){
        services.follow(user1 , user2).then((result)=>{
            response.send({
                status: "success",
                message: `user ${user2} followed successfully`
            })
            
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })     
        })  
    }
    else{
        response.status(400).send({
            status: "error",
            message : "you cant follow your self!!"
        }) 
    }
 
})

router.put("/unfollow" , authenticateJWT , (request , response)=>{
    let user1 = request.user.username;
    let user2 = request.query.username;
    if(user1 != user2){
        services.unfollow(user1 , user2).then((result)=>{
            response.send({
                status: "success",
                message: `user ${user2} unfollowed successfully`
            })
            
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
            
        })  
    }else{
        response.status(400).send({
            status: "error",
            message : "you cant unfollow your self!!"
        })  
    }
 
})

router.put("/admin/approveArtist" , (request , response)=>{
    let artist_id = request.query.user_id  
    services.approveArtist(artist_id).then(()=>{
        response.send({
            status: "success",
            message: `user ${artist_id} approved successfully`
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })      
})

router.post("/artist/addAlbum" , authenticateJWT , (request , response)=>{
    let username = request.user.username;
    services.isArtistApproved(username).then((id)=>{
        services.addAlbum({
            id,
            ...request.body
        }).then(()=>{
                response.send({
                    status: "success",
                    message: `album ${request.body.album_title} successfully added!`
                })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })        
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })       
})

router.delete("/artist/deleteAlbum" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    services.deleteAlbum(user_id , request.query.album_title).then(()=>{
        response.send({
            status: "success",
            message: `album successfully deleted!`
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })       
    
})  

router.post("/artist/addTarck" , authenticateJWT , (request , response)=>{            
            let data ={
                username : request.user.username,
                ...request.body
            }            
            services.addTrack(data).then(()=>{
                response.send({
                    status: "success",
                    message: `tracks successfully added!`
                })
            }).catch((error)=>{
                response.status(400).send({
                    status: "error",
                    message : error
                })
            })
})

router.delete("/artist/deleteTrack" , authenticateJWT , (request , response)=>{
    let username = request.user.username;
    let data = {
        username,
        ...request.body
    }
    services.deleteTrack(data).then(()=>{
        response.send({
            status: "success",
            message: `track successfully deleted!`
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })      
})


router.post("/listener/addTrack"  , authenticateJWT , (request , response)=>{  
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body
    }
    services.addTarckToPlaylist(data).then(()=>{
        response.send({
            status: "success",
            message: `track successfully added to ${data.playlist_title}`
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})

router.delete("/listener/deleteTrack" , authenticateJWT , (request ,response)=>{
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body
    }
    services.deleteTrackFromPlaylist(data).then(()=>{
        response.send({
            status: "success",
            message: `track successfully deleted from ${data.playlist_title}`
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})

router.post("/listener/reportTrack" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    let data = {
        user_id : user_id,
        ...request.body,
    }
    services.reportTrack(data).then(()=>{
        response.send({
            status: "success",
            message: `track successfully reported!`
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})

router.post("/listener/account/upgrade" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body
    }
    services.listenerUpgrade(data).then(()=>{
        response.send({
            status : "success",
            message: `user ${request.user.username} account upgraded successfully!`
        })
        
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message: error
        })
        
    })

    
})

router.post("/listener/playMusic" , authenticateJWT , (request , response)=>{
    
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body
    }
    //console.log(data)

    services.isPremiumOrLessThanFiveTracks(user_id).then(()=>{
        
        services.playTrack(data).then(()=>{
                response.send({
                    status: "success",
                    message: `track ${data.track_id} is playing!`
                })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })        
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })       

})

router.get("/listener/playlist" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    services.getPlaylist(user_id).then((result)=>{
        response.send({
            status : "success",
            data : result
        })
    }).catch((error)=>{
        if(error === "users have not any playlist!!"){
            response.send({
                status : "success",
                message : error 
            })
        }
        else{
            response.status(400).send({
                status: "error",
                message : error
            })
        }
       
    })
})

router.post("/listener/likeTrack" , authenticateJWT , (request , response)=>{
    
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body
    }    
    services.likeTrack(data).then(()=>{
        response.send({
            status : "success",
            message: `user ${request.user.username} liked track ${data.track_id}`
        })
        
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message: error
        })
        
    })

})

router.delete("/listener/unlikeTrack" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body
    }
    services.unlikeTrack(data).then(()=>{
        response.send({
            status : "success",
            message: `user ${request.user.username} unliked track ${data.track_id}`
        })
        
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message: error
        })
        
    })

})

//DONE add route for addPlaylist
router.post("/listener/addPlaylist" , authenticateJWT , (request , response)=>{
    
    let user_id = request.user.user_id;
    let data = {    
        user_id,
        ...request.body
    }
    services.isPremiumOrLessThanFivePlaylist(user_id).then(()=>{
        
        services.addPlaylist(data).then(()=>{
                response.send({
                    status: "success",
                    message: `playlist ${data.playlist_title} is added!`
                })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })        
    }).catch((error)=>{
        response.status(420).send({
            status: "error",
            message : error
        })
    })       

}) 

router.get("/account/followers" , authenticateJWT , (request , response)=>{    
    let user_id = request.user.user_id;
    // if want other user data user id come from query 
    if(request.query.id){ 
        user_id = request.query.id;
    }    
    services.getFollowers(user_id).then((data)=>{
        response.send({
            status: "success",
            message: `followers get successfully!`,
            data : data
        })

    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})


router.get("/account/following" , authenticateJWT , (request , response)=>{    
    let user_id = request.user.user_id;
    // if want other user data user id come from query  
    if(request.query.id){ 
        user_id = request.query.id;
    }
    services.getFollowing(user_id).then((data)=>{
        response.send({
            status: "success",
            message: `follwing get successfully!`,
            data : data
        })

    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})

router.get("/artist/albums" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    // if want other user data user id come from query 
    if(request.query.id)
        user_id = request.query.id;
    
    services.getAlbums(user_id).then((res)=>{
        response.send({
            status: "success",
            message: `user ${request.user.username} albums get successfully!`,
            data : res
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})


router.get('/artist/dominantGenre' , authenticateJWT , (request ,response)=>{
    let user_id = request.user.user_id;
    // if want other user data user id come from query 
    if(request.query.user_id)
        user_id = request.query.user_id;
    services.getDominanGenre(user_id).then((genre)=>{
        response.send({
            status: "success",
            message: `user ${user_id} dominant genre get successfully!`,
            data : {
                genre : genre
            }
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })

})           
router.post("/listener/likePlaylist" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body
    }
    services.likePlaylist(data).then(()=>{
        response.send({
            status : "success",
            message: `user ${request.user.username} liked playlist ${data.playlist_id}`
        })
        
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message: error
        })
        
    })

})

router.delete("/listener/unlikePlaylist" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body,    
        pedram : true
    }
    services.unlikePlaylist(data).then(()=>{                
        response.send({
            status : "success",
            message: `user ${request.user.username} unliked playlist ${data.playlist_id}`
        })
        
        
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message: error
        })
        
    })
    

})


router.get("/listener/track" , authenticateJWT , (request , response)=>{
    let data = {
        ...request.body
    }
    let track_id = data.track_id;
    services.getTrackInfo(track_id).then((result)=>{
        response.send({
            status : "success",
            data : result
        })
    }).catch((error)=>{
        if(error === "track not found!"){
            response.send({
                status : "error",
                message : error 
            })
        }
        else{
            response.status(400).send({
                status: "error",
                message : error
            })
        }
       
    })
})

router.get("/artist/popularTrack" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    // if want other user data user_id come from query 
    if(request.query.id)
        user_id = request.query.id;
    services.getArtistPopularTrack(user_id).then((data)=>{
        response.send({
            status: "success",
            message: `user ${user_id} popular track get successfully!`,
            data : {
                track : data
            }
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })    
})

router.get("/listener/suggestArtistsAlbumsWithSameNationality" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    if(request.query.id)
        user_id = request.query.id;
    services.getArtistsAlbumsWithSameNationality(user_id).then((data)=>{
        response.send({
            status: "success",
            message: `user ${user_id} Have Got Albums From Artists Of Same Nationality Succesfully!`,
            data 
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })    
})

router.post("/listener/sharePlaylist" , authenticateJWT , (request , response)=>{
    
    let user_id = request.user.user_id;
    let data = {
        user_id,
        ...request.body
    }
    services.isPremiumOrLessThanFivePlaylist(data.user_idS).then(()=>{
        
        services.sharePlaylist(data).then(()=>{
                response.send({
                    status: "success",
                    message: `user ${user_id} shared playlist ${data.playlist_title} with user ${data.user_idS}`
                })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })        
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })       

})


router.get("/search" , authenticateJWT , (request , response)=>{
    let searchQeury = request.query.q;
    if(searchQeury){
        services.search(searchQeury).then((data)=>{
            response.send({
                status: "success",
                message: `search done successfully!`,
                data : data
            })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : "nothing found!"
            })
        })
    }
    else {
        response.status(400).send({
            status: "error",
            message : "nothing entered!"
        })
    }
})

router.get("/admin/getSuspiciousRegularUsers" , (request , response)=>{    
    
    services.getSuspiciousRegularUsers().then((data)=>{
        response.send({
            status: "success",
            message: `suspicious regular listeners list get successfully!`,
            data : {
                suspicious_users: data
            }
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
    
})

router.get("/admin/artists" , (request , response)=>{    
    
    services.getArtistsOrderByActivity().then((data)=>{
        response.send({
            status: "success",
            message: `Artists list get successfully Order By Activity!`,
            data : {
                artists: data
            }
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
    
})

router.get("/listenre/lastFollowingPlay" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id
    services.getLastFollowingPlay(user_id).then((data)=>{
        response.send({
            status: "success",
            message: `get following last played track successfully!`,
            data : data
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
    
})

router.get("/listener/getNewTrackOfArtist" , authenticateJWT , (request, response)=>{
        let user_id = request.user.user_id;
        services.getNewTrackOfArtist(user_id).then((data)=>{
            response.send({
                status: "success",
                message: `get 5 new track of following artist successfully!`,
                data : data
            })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })
        
})

router.get("/listener/fivePopularTrackOfWeek" , authenticateJWT , (request , response)=>{    
    services.getFivePopularTrackOfWeek().then((data)=>{
        response.send({
            status: "success",
            message: `get 5 popular tracks of week successfully!`,
            data : data
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})

router.get("/listener/favoriteGenre" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    services.getListenerFavoriteGenre(user_id).then((data)=>{
        
        response.send({
            status: "success",
            message: `get popular genre of listener successfully!`,
            data : data
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})

router.get("/listener/getFivePopularTracksOfFavGenre",authenticateJWT , (request,response)=>{
    let user_id = request.user.user_id;
    services.getListenerFavoriteGenre(user_id).then((fav)=>{
        if(fav == 'nothing found'){
            response.send({
                status: "success",
                message: `get 5 popular tracks of favorite genre of listener successfully!`,
                data : []
            })
        }
        else{

        
        services.getFivePopularTracksOfFavGenre(fav).then((data)=>{
            response.send({
                status: "success",
                message: `get 5 popular tracks of favorite genre of listener successfully!`,
                data : data
            })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })
    }
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})

router.get("/listener/getFiveNewTracksOfFavGenre",authenticateJWT , (request,response)=>{
    let user_id = request.user.user_id;
    services.getListenerFavoriteGenre(user_id).then((fav)=>{
        if(fav == 'nothing found'){
            response.send({
                status: "success",
                message: `get 5 popular tracks of favorite genre of listener successfully!`,
                data : []
            })
        }
        else{
            services.getFiveNewTracksOfFavGenre(fav).then((data)=>{
                response.send({
                    status: "success",
                    message: `get 5 new tracks of favorite genre of listener successfully!`,
                    data : data
                })
            }).catch((error)=>{
                response.status(400).send({
                    status: "error",
                    message : error
                })
            })
        }
     
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})

router.get("/admin/artistsWithLowActivity" , (request , response)=>{    
    
    services.getLowActivityArtists().then((data)=>{
        response.send({
            status: "success",
            message: `Artists with low activity list get successfully!`,
            data : {
                artists: data
            }
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
    
})

router.get("/admin/getUsersListenedAtLeastOneMusicAday" , (request , response)=>{    
    
    services.getUsersListenedAtLeastOneMusicAday().then((data)=>{
        response.send({
            status: "success",
            message: `user_ids of listeners who listened to at least one music per day since their signup list get succesfully`,
            data : data
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
    
})


router.get("/admin/getUsersFollowUsersListenedAtLeastOneMusicAday" , (request , response)=>{    
    
    services.getUsersListenedAtLeastOneMusicAday().then((users)=>{
        //console.log(users[0].user_id)
        services.getUsersFollowSpeceficUsers(users).then((data)=>{
            response.send({
                status: "success",
                message: `user_ids of listeners who follow just listeners who listened to at least one music per day since their signup list get succesfully`,
                data : {
                    users: data
                }
            })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })

    
})

router.get("/listener/favoriteArtistOfListener" , authenticateJWT ,(request,response)=>{
    let user_id = request.user.user_id;
    services.getFavoriteArtistOfListener(user_id).then((artist)=>{
        
        response.send({
            status: "success",
            message: `favorite artist get successfully!`,
            data : artist
        })

    }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
    })
})

router.get("/listener/getArtistWithSameDomGenreWithfavoriteArtist" , authenticateJWT ,(request,response)=>{
    let user_id = request.user.user_id;
    services.getFavoriteArtistOfListener(user_id).then((artist)=>{                               
        services.getDominanGenre(artist.user_id).then((genre)=>{                                    
            services.getArtistWithSpecificDomGenre(artist.user_id,genre).then((res)=>{
                response.send({
                    status: "success",
                    message: `artists with same dom genre with favorite artist get successfully!`,
                    data : res
                })
            }).catch((error)=>{
                response.status(400).send({
                    status: "error",
                    message : error
                })
            })
            
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })
        
    }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
    })
})


router.get('/listenr/dominantPlayedGenre' , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    if(request.query.id)
        user_id = request.query.id;

    //console.log(user_id)
    services.getdominantGenreOflistenerplayed(user_id).then((genre)=>{
        response.send({
            status: "success",
            message: `Listener dominant genre  get successfully!`,
            data : {
                genre: genre
            }
        })
    
    })
})   


router.post("/listener/editPLaylist" , authenticateJWT , (request , response)=>{

    let user_id = request.user.user_id;
    let playlist_title = request.query.playlist_title;

    let data = {
        user_id,
        playlist_title,
        ...request.body
    }

    //console.log(data)
    if(playlist_title){
        services.editPLaylistName(data).then(()=>{
            response.send({
                status: "success",
                message: `user ${user_id} change playlist name from '${data.playlist_title}' to '${data.new_title}' successfully!`
            })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })
    }
    else {
        response.status(400).send({
            status: "error",
            message : "nothing entered!"
        })
    }
})



router.get("/admin/getALL" , (request , response)=>{
    services.adminGetALL().then((data)=>{
        response.send({
        status: "success",
        message: `requests are fetched successfully!`,
        data : data
        })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })
})
//for testing
router.get("/admin/getAllUsers" , (request , response)=>{    
    
    services.getAllUsers().then((data)=>{
        response.send({
            status: "success",
            message: `all users get successfully!`,
            data : data
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })    
})

router.get('/dominantGenrePlaylist' , authenticateJWT , (request ,response)=>{
    let data = {
        ...request.body
    }
    services.getDominanGenreOfPlaylist(data.playlist_id).then((genre)=>{
        response.send({
            status: "success",
            message: `dominant genre of playlist get successfully!`,
            data : {
                genre : genre
            }
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })
    })
})      


router.get("/artist/fans" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    services.getArtistFans(user_id).then((data)=>{
        response.send({
            status: "success",
            message: `get artist fans successfully!`,
            data : {
                users : data
            }
        })
    }).catch((error)=>{
        response.status(400).send({
            status: "error",
            message : error
        })        
    })   
})       

router.get('/suggestTwoTrackFromDominantGenreOfPlaylist' , authenticateJWT , (request ,response)=>{
    let playlist_id = request.query.playlist_id
    services.getDominanGenreOfPlaylist(playlist_id).then((genre)=>{       
                         
        services.suggestTwoTracksFromDominantGenre(genre ,playlist_id).then((twoTracks)=>{
            response.send({
                status: "success",
                message: `two tracks suggested succcesfully!`,
                data : {
                    tracks : twoTracks
                }
            })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })
    })
}) 

router.delete('/listener/deleteAccount' , authenticateJWT , (request ,response)=>{
    let data = {
        ...request.body
    }
    services.deleteAccount(data).then(()=>{
        response.send({
            status: "success",
            message: `account ${data.user_id} successfully deleted!`,
        })
        }).catch((error)=>{
            response.status(400).send({
                status: "error",
                message : error
            })
        })
    })

router.delete("/listener/deletePlaylist" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    let data ={
        user_id,
        playlist_id : Number(request.query.playlist_id)
    }
    services.listenerDeletePlaylist(data).then(()=>{
        response.send({
            status: "success",
            message: `playlist successfully deleted!`,
        })
    }).catch((error)=>{
        response.status(420).send({
            status: "error",
            message : error
        })
    })
    
})

router.get('/listener/getLikedTrack' , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    services.getLikedTracks(user_id).then((res)=>{
        response.send({
            status: "success",
            message: `get liked track successfully deleted!`,
            data: res
        })
    }).catch((error)=>{
        response.status(420).send({
            status: "error",
            message : error
        })
    })
})

router.get("/listener/likedPlaylist" , authenticateJWT , (request , response)=>{
    let user_id = request.user.user_id;
    services.getLikedPlaylist(user_id).then((res)=>{
        response.send({
            status: "success",
            message: `get liked playlist successfully!`,
            data: res
        })
    }).catch((error)=>{
        response.status(420).send({
            status: "error",
            message : error
        })
    })
})
    
router.get('/test' , (request , response)=>{
    let q = request.query.q
    services.test(q)    
})  

// router.get('/getUsers' , (request , resposne)=>{
//     services.getAllUsers().then((result)=>{
//         resposne.json(result);
//     }).catch((error)=>[
//         resposne.status(500).json({message : error.message})
//     ])
// })

module.exports = router; 

