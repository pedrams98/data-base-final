var mysql = require('mysql');
const crypto = require('crypto');
const { resolve } = require('path');
const e = require('express');
const { use } = require('../routes');
const { response, query, request } = require('express');
const router = require('../routes');
const { promises } = require('fs');
var pool  = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  user            : 'root',
  password        : '',
  database        : 'spotify_demo_db'
});

let services = {}

services.signup = (data)=>{
    return new Promise((resolve , reject)=>{
        let salt = createSalt();
        let hashedPassword = createPass(data.password , salt);   
        let date = getDate(0,0);        
        PromiseQuery(`INSERT INTO app_user (user_id, username, email, passwordHash , salt, firstname, lastname, nationality, user_type, birth_date , signup_date)
        VALUES 
        (NULL,'${data.username}', '${data.email}', '${hashedPassword}' , '${salt}' , '${data.firstname}',
         '${data.lastname}', '${data.nationality}', '${data.user_type}', '${data.birth_date}', '${date}')`).then((res)=>{
                let user_id = res.insertId; 
                PromiseQuery(`INSERT INTO security_answers (ans1 , ans2 , ans3 , user_id) VALUE ('${data.ans1}','${data.ans2}','${data.ans3}', '${user_id}')`).catch((error)=>{
                    reject(error)
                })
                if(data.user_type == "A"){
                    PromiseQuery(`INSERT INTO artist (artistic_name , activity_start_data , user_id ) VALUE 
                    ('${data.artistic_name}' , '${data.activity_start_data}' , '${user_id}')`).then(()=>{
                        PromiseQuery(`INSERT INTO disapproved_artist (user_id) VALUE ('${user_id}')`).then(()=>{
                            resolve()
                        }).catch((error)=>{
                            reject(error)
                        })
                    }).catch((error)=>{
                        reject(error)
                    })
                }
                else{
                    PromiseQuery(`INSERT INTO playlist (playlist_title , user_id) VALUE ('${"liked"}' , '${user_id}')`).then(()=>{
                        resolve();
                    })
                    .catch((error)=>{
                        reject(error)
                    })
                }
         }).catch((error)=>{
            reject(error)
         })    
       

        // pool.query(`INSERT INTO app_user (user_id, username, email, passwordHash , salt, firstname, lastname, nationality, user_type, birth_date , signup_date)
        //  VALUES 
        //  (NULL,'${data.username}', '${data.email}', '${hashedPassword}' , '${salt}' , '${data.firstname}', '${data.lastname}', '${data.nationality}', '${data.user_type}', '${data.birth_date}', '${date}')`, (error ,result)=>{
        //     if(error){
        //         reject(error)
        //     }
        //     else{
        //         getUserID(data.username , data.email).then((id)=>{
        //             pool.query(`INSERT INTO security_answers (ans1 , ans2 , ans3 , user_id) VALUE ('${data.ans1}','${data.ans2}','${data.ans3}', '${id}')` , (error , result)=>{
        //                 if(error)
        //                     reject(error)         
        //             })
        //             pool.query(`INSERT INTO playlist (playlist_title , user_id) VALUE ('${"liked"}' , '${id}')` , (error , result)=>{
        //                 if(error)
        //                     reject(error)         
        //             })
        //             if(data.user_type == "A"){ 
        //                 let addDataToArtistTable =  pool.query(`INSERT INTO artist (artistic_name , activity_start_data , user_id ) VALUE ('${data.artistic_name}' , '${data.activity_start_data}' , '${id}')`);
        //                 let addDataToDisapprovedArtistTable = pool.query(`INSERT INTO disapproved_artist (user_id) VALUE ('${id}')`);
        //                 let promisses = [addDataToArtistTable , addDataToDisapprovedArtistTable];
        //                 Promise.all(promisses).then(()=>{
        //                     resolve() 
        //                 }).catch((error)=>{
        //                     reject(error)
        //                 })
        //             }
        //             else
        //                 resolve();
        //         })
        //     }    
         
        // })
    })
} 

services.login = (data)=>{    
    let type2;
    if(data.user_type === "R")
        type2 = "P"        
    return new Promise((resolve , reject)=>{
        pool.query(`SELECT user_id, passwordHash , salt FROM app_user WHERE username = "${data.username}" AND (user_type = "${data.user_type}" OR user_type="${type2}")` ,(error , result)=>{
            if(error)
                reject("error during query!");                
            if(result[0]){
                let id = result[0].user_id
                let existPass = result[0].passwordHash;
                let salt = result[0].salt;
                if(validPass(data.password , existPass , salt)){ 
                    if(data.user_type === "A"){
                        resolve(id)
                    }                   
                    isUserNotPremium(id).then(()=>{
                        resolve(id)
                    }).catch((error)=>{                        
                        if(error === "user is already is premium!"){
                           PromiseQuery(`SELECT membership_till FROM premium_listener WHERE user_id = '${id}'`).then((result)=>{
                                let tillDate = result[0].membership_till;
                                if(tillDate < Date.now()){                                    
                                    PromiseQuery(`UPDATE app_user SET user_type = 'R' WHERE user_id = '${id}'`).then(()=>{
                                        resolve(id)
                                    }).catch((error)=>{
                                        reject(error)
                                    })
                                }
                                else
                                    resolve(id)
                           }).catch((error)=>{
                               reject(error)
                           })
                            
                        }
                        else 
                            reject(error)

                    })

                }
                else 
                    reject("incorrect pass"); 
            }
            else
                reject("user not found!!");     
        })
    })
   
}

services.verifySecurityAnswers = (data)=>{
    return new Promise((resolve , reject)=>{
        getUserID("" , data.email).then((id)=>{
            pool.query(`SELECT * FROM security_answers WHERE (ans1 = "${data.ans1}" AND ans2 = "${data.ans2}" AND ans3 = "${data.ans3}" AND user_id = "${id}" )` , (error , result)=>{
                if(error)
                    reject("error during query!");
                else{
                    if(result[0])
                        resolve(id)
                    else   
                        reject("user not found!!")    
                }
            })
        }).catch(()=>{
            reject("user not found!")
        })
    })
}

services.changePassword = (data)=>{
    return new Promise((resolve , reject)=>{
        let salt = createSalt();
        let hashedPassword = createPass(data.new_password , salt);
        pool.query(`UPDATE app_user SET passwordHash = "${hashedPassword}" , salt = "${salt}" WHERE user_id ="${data.user_id}"` , (error , result)=>{
            if(error)
                reject("error during query!")                
            else{
                if(result)
                    resolve()
                else    
                    reject("user not found!!")   
            }   
        })  
    })
  
};   


services.getProfile = (id)=>{
    return new Promise((resolve, reject)=>{
        pool.query(`SELECT user_id , username , email , firstname , lastname , nationality , user_type , birth_date FROM app_user WHERE (user_id = "${id}" )` , (error , result)=>{            
            if(error)
                reject("error during query!!");
            else {
                let accountInfo = result[0];
                if(accountInfo){
                    if(accountInfo.user_type == "A"){
                            PromiseQuery(`SELECT artistic_name , activity_start_data FROM artist WHERE user_id = "${id}"`).then((result)=>{
                                if(result[0]){
                                    let artisticInfo = result[0]
                                    services.getDominanGenre(id).then((dominant_genre)=>{                                        
                                        services.getArtistPopularTrack(id).then((popular_track)=>{
                                            services.getAlbums(id).then((albums)=>{
                                                services.getFollowers(id).then((followers)=>{
                                                    services.getFollowing(id).then((following)=>{
                                                        resolve({
                                                            ...accountInfo,
                                                            ...artisticInfo,
                                                            dominant_genre : dominant_genre,
                                                            popular_track : popular_track,
                                                            albums : albums ,
                                                            followers : followers,
                                                            following : following
                                                        })
                                                    }).catch((error)=>{
                                                        reject(error);
                                          
                                                    })
                                                }).catch((error)=>{
                                                    reject(error);
                                                    
                                                })
                                            }).catch((error)=>{
                                                reject(error);
                                                
                                            })
                                        }).catch((error)=>{
                                            reject(error);
                                            
                                        })
                                    }).catch((error)=>{
                                        reject(error);
                                        
                                    })
                                }
                                else{
                                    reject("artistic inforamtion not found!!")
                                }
                            }).catch((error)=>{
                                reject(error);
                            })
                         
                    }
                    else{
                        
                        services.getPlaylist(id).then((playlists)=>{
                            // console.log("inja");
                            services.getFollowers(id).then((followers)=>{
                                services.getFollowing(id).then((following)=>{
                                    resolve({
                                        ...accountInfo,
                                        playlists : playlists,
                                        followers : followers,
                                        following : following
                                    });
                                }).catch((error)=>{
                                    reject(error)
                                })
                              
                            }).catch((error)=>{
                                reject(error)
                            })
                        }).catch(error=>{
                            reject(error)
                        })
                        
                    }
                }
                else{
                    reject("user not found!!")
                }
    
            
            }
            
            
        })
    })
}

services.follow =(user1 , user2)=>{    
    return new Promise((resolve , reject)=>{  
        getUserID(user1 , "").then((id1)=>{
            getUserID(user2 , "").then((id2)=>{
                pool.query(`INSERT INTO follow (user_id , following_id) VALUE ('${id1}' , '${id2}'  )` , (error , result)=>{
                    if(error)
                        reject(error)
                    else
                        resolve(result)    
                })
            }).catch(()=>{
                reject("user not found!!")
            })
        }).catch(()=>{
            reject("can not get your id!!")
        })
    })   
}

services.unfollow = (user1 , user2)=>{
    return new Promise((resolve , reject)=>{
        getUserID(user1 , "").then((id1)=>{
            getUserID(user2 , "").then((id2)=>{
                pool.query(`DELETE FROM follow WHERE (user_id = "${id1}" AND following_id = "${id2}")` , (error , result)=>{
                    if(error)
                        reject(error)
                    else{
                        if(result.affectedRows)
                            resolve()
                        else 
                            reject("user not found!")
                    }
                })
            }).catch(()=>{
                reject("user not found!!")
            })
        }).catch(()=>{
            reject("can not get your id!!")
        })
    })
}

services.approveArtist = (id)=>{
    return new Promise((resolve , reject)=>{
        pool.query(`DELETE FROM disapproved_artist WHERE user_id = "${id}"` , (error, result)=>{
            if(error)
                reject(error)
            else {                
                if(result.affectedRows)
                    resolve();
                else    
                    reject("user not found!!");
            }    
        })
    })
}

services.isArtistApproved = (username)=>{
    return new Promise((resolve , reject)=>{
        getUserID(username , "").then((id)=>{
            pool.query(`SELECT user_id FROM disapproved_artist WHERE user_id = "${id}"` , (error , result)=>{
                if(error)
                    reject(error);
                else{
                    if(result[0])
                        reject("user not approved!")
                    else    
                        resolve(id)    
                }              
            })
        }).catch(()=>{
            reject("user not found!!")
        })
    })
}

services.addAlbum = (data)=>{

    
    return new Promise((resolve , reject)=>{
        pool.query(`INSERT INTO album (album_id , album_title ,release_date, user_id) VALUES (NULL , '${data.album_title}' ,'${data.release_date}', '${data.id}')`, (error , result)=>{
            if(error)
                reject(error)
            else{        
                if(data.tracks.length ==1){
                    //     PromiseQuery(`INSERT INTO track (track_id , track_title , track_duration , album_id) VALUES (NULL , '${data.album_title}','${data.tracks[0].track_duration}','${result.insertId}')`)
                    // .then(()=>{
                    //     resolve()
                    // }).catch(()=>{
                    //     reject()
                    // })
                    PromiseQuery(`INSERT INTO track (track_id , track_title , genre , track_duration , album_id) VALUES (NULL , '${data.album_title}','${data.tracks[0].genre}','${data.tracks[0].track_duration}','${result.insertId}')`).then(()=>{
                        PromiseQuery(`INSERT IGNORE INTO genre (genre_title , album_id) VALUES ('${data.tracks[0].genre}' , '${result.insertId}')`).then(()=>{
                            resolve()
                        }).catch((error)=>{
                            reject(error)
                        })
                    }).catch((error)=>{
                        reject(error)
                    })
                }
                else{                    
                    data.tracks.forEach((track , index  ,tracks)=>{
                        PromiseQuery(`INSERT INTO track (track_id , track_title, genre , track_duration , album_id) VALUES (NULL , '${track.track_title}' , '${track.genre}' , '${track.track_duration}' , '${result.insertId}')`)
                    .then(()=>{
                        pool.query(`INSERT IGNORE INTO genre (genre_title , album_id) VALUES ('${track.genre}' , '${result.insertId}')`)
                        if(index == tracks.length -1 )
                            resolve()
                    }).catch((error)=>{
                        reject(error)
                    })  
                    })
                }
            }    
        })
    })    

}

services.deleteAlbum = (user_id , album_title)=>{
    return new Promise ((resolve  , reject)=>{
            getAlbumID(album_title , user_id).then((album_id)=>{
                pool.query(`DELETE FROM album WHERE (album_id='${album_id}' AND user_id='${user_id}')` , (error ,result)=>{
                    
                    if(error)
                        reject(error)
                    else{
                        if(result.affectedRows)
                            resolve()
                        else
                            reject("album not found!")    
                    }
                })                

            }).catch((error)=>{
                reject(error);
            })
    })
}


services.addTrack = (data)=>{    
    return new Promise((resolve , reject)=>{
        getUserID(data.username).then((user_id)=>{
            getAlbumID(data.album_title , user_id).then((album_id)=>{
                data.tracks.forEach((track , index , tracks)=>{    
                    PromiseQuery(`INSERT INTO track (track_id , track_title , track_duration , album_id) VALUES (NULL , '${track.track_title}' , '${track.track_duration}' , '${album_id}')`)
                    .then(()=>{
                        pool.query(`INSERT IGNORE INTO genre (genre_title , album_id) VALUES ('${track.genre}' , '${album_id}')`)
                        if(index == tracks.length -1 )
                            resolve()
                    }).catch((error)=>{
                        reject(error)
                    })
                })
              
            }).catch((error)=>{
                reject(error);
            })
        }).catch((error)=>{
            reject(error);
        })
    })
}

services.deleteTrack = (data)=>{
    return new Promise((resolve , reject)=>{        
        getUserID(data.username).then((user_id)=>{
            getAlbumID(data.album_title , user_id).then((album_id)=>{
                pool.query(`DELETE FROM track WHERE (track_title='${data.track_title}' AND album_id='${album_id}')` , (error ,result)=>{
                    if(error)
                        reject(error)
                    else{
                        if(result.affectedRows)
                            resolve()
                        else
                            reject("track not found!")    
                    }
                })                

            }).catch((error)=>{
                reject(error);
            })
        }).catch((error)=>{
            reject(error);
        })
    })    
}

services.addTarckToPlaylist = (data)=>{
    return new Promise((resolve , reject)=>{
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = yyyy + '-' + mm + '-' + dd;

        pool.query(`SELECT playlist_id  FROM playlist WHERE (playlist_title  = '${data.playlist_title}' AND user_id ='${data.user_id}')` , (error , result)=>{
            if(error)
                reject(error);
            else{
                if(result[0]){
                    let playlist_id = result[0].playlist_id;
                    pool.query(`INSERT INTO playlist_track (track_id  , playlist_id , added_date) VALUES ('${data.track_id}' , '${playlist_id}' , '${today}')` , (error , result)=>{
                        if(error)
                            reject(error)
                        else
                            resolve()    
                    } )
                }   
                else  
                    resolve("playlist not found!!")
            } 
                
        })        
    })    
}   

services.deleteTrackFromPlaylist = (data)=>{
    return new Promise((resolve , reject)=>{
        
        
        pool.query(`SELECT playlist_id  FROM playlist WHERE (playlist_title  = '${data.playlist_title}' AND user_id ='${data.user_id}')` , (error , result)=>{
            if(error)
                reject(error);
            else{
                if(result[0]){
                    let playlist_id = result[0].playlist_id;
                    pool.query(`DELETE FROM playlist_track WHERE (track_id = '${data.track_id}' AND playlist_id = '${playlist_id}')` , (error ,result)=>{
                        if(error)
                        reject(error)
                    else{
                        if(result.affectedRows)
                            resolve()
                        else
                            reject("track not found!")    
                    }
                    })
                }   
                else  
                    reject("playlist not found!!")
            } 
                
        })        
    })    
}   


services.reportTrack= (data)=>{
    return new Promise((resolve , reject)=>{
        pool.query(`INSERT INTO report (report_message , user_id , track_id) VALUES ('${data.message}' , '${data.user_id}' , '${data.track_id}')` , (error,result)=>{
            if(error)
                // reject(error)
                reject(error)
                
            else    
                resolve();
                
        })
    })
    
}

services.listenerUpgrade = (data)=>{
    return new Promise((resolve , reject)=>{
        isUserNotPremium(data.user_id).then(()=>{            
            let membership_from = getDate();
            let membership_tail = getDate(data.month_count) 
                          
            Promise.all([
                PromiseQuery(`INSERT INTO premium_listener (membership_from , membership_till , user_id) VALUES ('${membership_from}' , '${membership_tail}' , '${data.user_id}')`),
                PromiseQuery(`INSERT INTO credit_card (card_number , expiration_date , user_id) VALUES ('${data.card_number}' , '${data.expiration_date}' , '${data.user_id}')`),
                PromiseQuery(`UPDATE app_user SET user_type = 'P' WHERE user_id = '${data.user_id}'`),
            ]).then(()=>{
                resolve()
            }).catch((error)=>{
                reject(error)
            })
                    
        }).catch((err)=>{
            reject(err)            
        })
    })
    
}

services.playTrack = (data)=>{
    return new Promise((resolve , reject)=>{
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var date = yyyy + '-' + mm + '-' + dd;

        
        var hh = String(today.getHours() + 1);
        var mm = String(today.getMinutes() + 1).padStart(2, '0');
        var ss = String(today.getSeconds() + 1).padStart(2, '0');
        var time = hh + ':' +  mm + ':' + ss;

        //console.log(data)
        pool.query(`INSERT INTO play (track_id ,user_id , last_played_date , last_played_time) VALUES ('${data.track_id}' , '${data.user_id}' , '${date}' , '${time}' )` , (error , result)=>{
                        if(error)
                            reject(error)
                        else
                            resolve()    
        })

    })
}

services.isPremiumOrLessThanFiveTracks = (user_id)=>{
    //console.log(user_id)
    return new Promise((resolve , reject)=>{
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = yyyy + '-' + mm + '-' + dd;


        pool.query(`SELECT user_type FROM app_user WHERE (user_id = "${user_id}")` , (error , result)=>{
            if(error)
                reject(error);
            else{
                let user_type = result[0].user_type;
                //console.log(user_type)
                if(user_type == "P"){
                    //console.log("is premium")
                    resolve()
                }
                else if(user_type == "R"){

                    pool.query(`SELECT * FROM play WHERE (( SELECT COUNT(*) FROM play WHERE ( user_id = '${user_id}' AND last_played_date = '${today}')) > 4)  `, (error , result)=>{
                        if(error)
                            reject(error);
                        else{
                            if(result[0])
                                reject("you cant listen to more than 5 musics a day,Go premium for no limits!")
                            else
                                resolve();
                        }
                    })
                }
                else
                    reject("user type is invalid")    
            }              
        })

    })
}

services.sharePlaylist = (data)=>{
    
    return new Promise((resolve , reject)=>{

        pool.query(`SELECT playlist_id FROM playlist WHERE (user_id = "${data.user_id}" AND playlist_title = "${data.playlist_title}")` , (error , result)=>{
            if(error)
                reject(error);
            else{
                let playlist_id = result[0].playlist_id;
                if(data.playlist_title == "liked"){
                    pool.query(`INSERT INTO playlist (playlist_id , user_id , playlist_title) VALUES ('${playlist_id}' , '${data.user_idS}' , '${"likedShared"}')` , (error , result)=>{
                        if(error)
                            reject(error)
                        else
                            resolve()    
                    })
                }
                else{
                    pool.query(`INSERT INTO playlist (playlist_id , user_id , playlist_title) VALUES ('${playlist_id}' , '${data.user_idS}' , '${data.playlist_title}')` , (error , result)=>{
                        if(error)
                            reject(error)
                        else
                            resolve()    
                    })
                }
                
            }              
        })
    })
}

services.getPlaylist = (user_id)=>{            
    return new Promise((resolve , reject)=>{        
        PromiseQuery(`SELECT playlist_id , playlist_title from playlist WHERE user_id ='${user_id}'`).then((result)=>{            
            if(result.length > 0){
                let data = JSON.parse(JSON.stringify(result))
                let res = []
                let bool = false;
                services.getPlaylistAndTracks(user_id).then((result)=>{                    
                        data.forEach(all=>{                            
                            bool = false;
                            result.forEach((each , index , array)=>{
                                if(all.playlist_id == each.playlist_id){
                                    res.push(each)
                                    bool = true
                                }
                            })
                            if(!bool){
                                res.push({
                                    playlist_id :all.playlist_id,
                                    playlist_title : all.playlist_title,
                                    track_count :0,
                                    tracks : []
                                 })
                            }
                        })
                        resolve(res)
                                            
                }).catch((error)=>{
                    reject(error);
                    
                })    
            }
             
        }).catch((error)=>{
            reject(error)
        })
    })
}

services.getPlaylistAndTracks = (user_id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT * FROM playlist
        INNER JOIN playlist_track ON playlist.playlist_id = playlist_track.playlist_id
        INNER JOIN track ON playlist_track.track_id = track.track_id
        INNER JOIN album ON track.album_id = album.album_id 
        INNER JOIN artist ON album.user_id = artist.user_id
        WHERE playlist.user_id = '${user_id}'`).then((result)=>{
            let data = result;
            
            if(data.length == 0){
                resolve([])
            }
 
            let playlist_ids = []
            data.forEach(item=>{
                if(!playlist_ids.includes(item.playlist_id))
                    playlist_ids.push(item.playlist_id)
            })
            let res = []
            playlist_ids.forEach(id=>{
                let temp = {
                    playlist_id : null,
                    playlist_title : null,
                    track_count : null,
                    tracks : []
                }
                temp.playlist_id = id;
                data.forEach(item=>{
                    
                    if(item.playlist_id == id){
                        if(temp.playlist_title === null){
                            temp.playlist_title = item.playlist_title;
                     
                        }
                        temp.tracks.push({
                            track_id : item.track_id,
                            track_title : item.track_title,
                            track_duration :item.track_duration,
                            genre : item.genre,
                            added_date : item.added_date,
                            album_id : item.album_id,
                            album_title: item.album_title,
                            artist_name : item.artistic_name
                        })
                    }   
                })
                temp.track_count = temp.tracks.length
                res.push(temp)
            })         
            resolve(res)
            
        }).catch((error)=>{
            reject(error)
        })
    })
}

services.likeTrack = (data)=>{    
    return new Promise((resolve , reject)=>{
        
        // pool.query(`SELECT playlist_id FROM playlist WHERE (user_id = '${data.user_id}' AND playlist_title = '${"liked"}')` , (error , result)=>{
        //                 if(error)
        //                     reject(error)
        //                 else
        //                     if(result[0]){
                                // let playlist_id = result[0].playlist_id
                                // let today = getDate();

        //                         let addDataTolikedTrackTable =  pool.query(`INSERT INTO liked_track (track_id , playlist_id , user_id) VALUES ('${data.track_id}' , '${playlist_id}' , '${data.user_id}')`);
        //                         let addDataToplaylist_trackTable = pool.query(`INSERT INTO playlist_track (track_id , playlist_id , added_date) VALUE ('${data.track_id}' , '${playlist_id}' , '${today}')`);
        //                         let promisses = [addDataTolikedTrackTable , addDataToplaylist_trackTable];
        //                         Promise.all(promisses).then(()=>{
        //                             resolve() 
        //                         }).catch((error)=>{
        //                             reject(error)
        //                         })

        //                     }
        //                     else{
        //                         //if user doesnt have liked playlist
        //                         reject("User doesnt have liked playlist")
        //                     }
                                    
        // })
        PromiseQuery(`SELECT playlist_id FROM playlist WHERE (user_id = '${data.user_id}' AND playlist_title = '${"liked"}')`).then((result)=>{                
                if(result[0]){
                    let playlist_id = result[0].playlist_id
                    let today = getDate();  
                                      
                    PromiseQuery(`INSERT INTO liked_track (track_id ,liked_date , playlist_id , user_id) VALUES ('${data.track_id}' ,'${today}', '${playlist_id}' , '${data.user_id}')`).then(()=>{                        
                        PromiseQuery(`INSERT INTO playlist_track (track_id , playlist_id , added_date) VALUE ('${data.track_id}' , '${playlist_id}' , '${today}')`).then(()=>{
                            console.log("inja");
                            
                            resolve()
                        }).catch((error)=>{
                            reject(error)
                        })
                    }).catch((error)=>{
                        reject(error)
                    })
                }
                else{
                    services.addPlaylist({user_id: data.user_id , playlist_title:'liked'}).then((playlist_id)=>{
                        let today = getDate();                    
                        PromiseQuery(`INSERT INTO liked_track (track_id , playlist_id , user_id) VALUES ('${data.track_id}' , '${playlist_id}' , '${data.user_id}')`).then(()=>{                        
                            PromiseQuery(`INSERT INTO playlist_track (track_id , playlist_id , added_date) VALUE ('${data.track_id}' , '${playlist_id}' , '${today}')`).then(()=>{
                                resolve()
                            }).catch((error)=>{
                                reject(error)
                            })
                        }).catch((error)=>{
                            reject(error)
                        })
                        
                    })

                }
        }).catch((error)=>{
            reject(error)
        })

    })
}

services.unlikeTrack = (data)=>{
    return new Promise((resolve , reject)=>{
        pool.query(`SELECT playlist_id FROM playlist WHERE (user_id = '${data.user_id}' AND playlist_title = '${"liked"}') ` , (error , result)=>{
                        if(error)
                            reject(error)
                        else
                            if(result[0]){
                                let playlist_id = result[0].playlist_id
                                PromiseQuery(`DELETE FROM liked_track WHERE (track_id= '${data.track_id}' AND playlist_id= '${playlist_id}' AND user_id = '${data.user_id}')`).then((res)=>{
                                    if(res.affectedRows > 0){
                                        PromiseQuery(`DELETE FROM playlist_track WHERE (track_id = '${data.track_id}' AND playlist_id = '${playlist_id}')`).then(()=>{
                                            resolve()
                                        }).catch((error)=>{
                                            reject(error)
                                        })
                                    }
                                    else{
                                        reject("track not liked!")
                                    }
                                }).catch((error)=>{
                                    reject(error)
                                    
                                })

                            }
                            else{
                                //if user doesnt have liked playlist
                                reject("User doesnt have liked playlist")
                            }
                                    
        })

    })
}

//DONE api for inserting playlist
services.addPlaylist = (data)=>{    
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT * FROM playlist WHERE (user_id = '${data.user_id}' AND playlist_title= '${data.playlist_title}')`).then((result)=>{
            if(result.length == 0){
                PromiseQuery(`INSERT INTO playlist (playlist_title, user_id) VALUES ('${data.playlist_title}' , '${data.user_id}' )`).then((res)=>{
                    resolve(res.insertId)
                }).catch((error)=>{
                    reject(error)
                })
            }else{
                reject("user have a playlist with same name!")
            }
            
        }).catch((error)=>{
            reject(error)
            
        })
        // pool.query(`INSERT INTO playlist (playlist_title, user_id) VALUES ('${data.playlist_title}' , '${data.user_id}' )` , (error , result)=>{
        //                 if(error)
        //                     reject(error)
        //                 else
        //                     resolve()    
        // })

    })
}

// DONE api fot cheking the number of playlists with respect to the user_type
services.isPremiumOrLessThanFivePlaylist = (user_id)=>{
    
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT user_type FROM app_user WHERE (user_id = "${user_id}") `).then((result)=>{
            if(result[0].user_type == 'P'){                
                resolve()
            }
            else{
                PromiseQuery(`SELECT * FROM playlist WHERE user_id = '${user_id}'`).then((result)=>{
                    if(result.length <5)
                        resolve()
                    else    
                        reject("you cant add  more than 5 playlists ,Go premium for no limits!")    
                }).catch((error)=>{
                    reject(error)
                })
            }    
            
        }).catch((error)=>{
            reject(error)
        })
        // pool.query(`SELECT user_type FROM app_user WHERE (user_id = "${user_id}") ` , (error , result)=>{
        //     if(error)
        //         reject(error);
        //     else{
        //     resolve();
        //     }              
        // })

    })
}
//DONE add api for likeplaylist
//DONE check number of playlists not more than five and insert into playlist table too
services.likePlaylist = (data)=>{    
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT user_type FROM app_user WHERE user_id = '${data.user_id}'`).then((resUserType)=>{            
            let uType  = resUserType[0].user_type;                        
            if(uType == "R") {
            PromiseQuery(`SELECT * FROM playlist WHERE user_id = '${data.user_id}'`).then((result)=>{                                
                if(result.length < 5) {    
                    let bool = true;
                    result.forEach(item=>{
                        if(item.playlist_id == Number(data.playlist_id))
                            bool = false
                    })                           
                    if(bool){
                        PromiseQuery(`INSERT INTO liked_playlist (user_id, playlist_id) 
                        VALUES ('${data.user_id}', '${data.playlist_id}')`).then(()=>{   
                                    PromiseQuery(` SELECT playlist_title , username
                                    FROM playlist 
                                    INNER JOIN app_user ON playlist.user_id = app_user.user_id
                                    WHERE (playlist_id = ${data.playlist_id})`).then((res)=>{
                                        
                                        let title = res[0].playlist_title;
                                        if(title == "liked"){
                                            title = title + res[0].username
                                        }                                        
                                        PromiseQuery(`INSERT INTO playlist (playlist_id , playlist_title , user_id) VALUES ('${data.playlist_id}','${title}','${data.user_id}')`).then(()=>{
                                        resolve()
                                        }).catch((error)=>{
                                            reject(error)
                                        })
                                        
                                    }).catch((error)=>{
                                        reject(error)
                                    })                  
                                    // PromiseQuery(`INSERT INTO playlist (playlist_id, playlist_title, user_id) 
                                    // SELECT playlist_id, playlist_title, ${data.user_id} 
                                    // FROM playlist 
                                    // WHERE (playlist_id = ${data.playlist_id})`).then((res)=>{
                                    //     resolve()
                                    // }).catch((error)=>{
                                    //     reject("REGULAR: couldn't insert in to playlist")
                                    // })
                        }).catch((error)=>{
                            reject("REGULAR: couldn't insert in to liked playlist")
                            
                        })
                    }
                    else{
                        reject("you cant like your playlist")
                    }
                }
                else {
                    reject("more than five playlists!! go premium for no limits!")
                }                
            }).catch((error)=>{
                reject("user not found")
                
            })
        }
        else if(uType == "P") {                        
            PromiseQuery(`SELECT * FROM playlist WHERE user_id = '${data.user_id}'`).then((result)=>{                
                let bool = true;
                    result.forEach(item=>{
                        if(item.playlist_id == Number(data.playlist_id))
                            bool = false
                    })                                                            
                    if(bool){
                        PromiseQuery(`INSERT INTO liked_playlist(user_id, playlist_id) 
                        VALUES(${data.user_id}, ${data.playlist_id})`).then(()=>{
                                PromiseQuery(` SELECT playlist_title
                                 FROM playlist 
                                 WHERE (playlist_id = ${data.playlist_id})`).then((res)=>{
                                     let title = res[0].playlist_title;
                                     PromiseQuery(`INSERT INTO playlist (playlist_id , playlist_title , user_id) VALUES ('${data.playlist_id}','${title}','${data.user_id}')`).then(()=>{
                                        resolve()
                                     }).catch((error)=>{
                                         reject(error)
                                     })
                                     
                                 }).catch((error)=>{
                                     reject(error)
                                 })
                                // PromiseQuery(`INSERT INTO playlist (playlist_id, playlist_title, user_id) 
                                // SELECT playlist_id, playlist_title, ${data.user_id} 
                                // FROM playlist 
                                // WHERE (playlist_id = ${data.playlist_id})`).then((res)=>{
                                    
                                //     resolve()
                                // }).catch((error)=>{
                                //     console.log(error);

                                //     reject("PREMIUM: couldn't insert in to playlist")
                                // })
                        }).catch((error)=>{
                            
                            reject("PREMIUM: couldn't insert in to liked playlist")
                        })
                    }
                    else{
                        reject("you cant like your playlist")
                    }  
            }).catch((error)=>{
                reject(error)
            })                               

         
        }
        }).catch((error)=>{
            reject("user not found")
        })
    })
}




// DONE add api for unlikeplaylist
//DONE delete from playlist table too
// cascades
services.unlikePlaylist  = (data)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`DELETE FROM liked_playlist WHERE (user_id= '${data.user_id}' AND playlist_id= '${Number(data.playlist_id)}')`).then((res)=>{                                   
            if(res.affectedRows > 0 ){
                PromiseQuery(`DELETE FROM playlist WHERE (user_id= '${data.user_id}' AND playlist_id= '${Number(data.playlist_id)}')`).then(()=>{                                        
                    resolve()
                }).catch((error)=>{
                    reject(error)
                })
            }   
            else {
                reject("playlist not liked!")
            }
        }).catch((error)=>{
            reject(error)
        })

    })
}


services.getTrackInfo = (track_id)=>{
    return new Promise((resolve, reject)=>{
        pool.query(`SELECT track_title , genre , track_duration , album_id FROM track WHERE track_id = "${track_id}"` , (error , result)=>{
            if(error)
                reject("Query failed!");
            else {
                resolve(result[0]);
            }
        })
    })
  
}

services.getTrackInfo = (track_id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT track_title , genre , track_duration , album_id FROM track WHERE track_id = "${track_id}"`).then((result)=>{
            if(result.length == 0)
                reject("track not found!")
            resolve(result);
            
        }).catch((error)=>{
            reject(error)
            
        })
    })
}
            
services.getFollowers = (id)=>{
    return new Promise((resolve , reject)=>{
        
        PromiseQuery(`SELECT follow.user_id , username , firstname , lastname , nationality FROM follow
        INNER JOIN app_user on app_user.user_id = follow.user_id
        WHERE following_id = '${id}'`).then((result)=>{
            
            if(result[0]){                
                let data = []
                resolve(result)
            }
            else{
                resolve([])
            }
            
        }).catch((error)=>{
            reject(error)
            
        })
    })
}
services.getFollowing = (id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT follow.following_id as user_id , username , firstname , lastname , nationality  FROM follow 
        INNER JOIN app_user ON app_user.user_id = follow.following_id
        WHERE follow.user_id = '${id}'`).then((result)=>{    
            if(result[0]){                
                resolve(result)
            }
            else{
                resolve([])
            }
            
        }).catch((error)=>{
            reject(error)
            
        })
    })
}



services.getAlbums = (user_id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT * FROM  
            album INNER JOIN track ON album.album_id = track.album_id
            WHERE album.user_id = '${user_id}'`).then((result)=>{
                if(result.length > 0){
                    let data = result;

                    let ids = []
                    data.forEach(item=>{
                        if(!ids.includes(item.album_id))
                            ids.push(item.album_id)
                    })
                    let res = []
                    ids.forEach(id=>{
                        let temp = {
                            album_id : null,
                            album_title : null,
                            track_count : null,
                            tracks : []
                        }
                        temp.album_id = id;
                        data.forEach(item=>{
                            if(item.album_id == id){
                                if(temp.album_title === null){
                                    temp.album_title = item.album_title;
                                }
                                temp.tracks.push({
                                    track_id : item.track_id,
                                    track_title : item.track_title,
                                    track_duration :item.track_duration,
                                    genre : item.genre,
                                    added_date : item.added_date,
                                })
                            }   
                        })
                        temp.track_count = temp.tracks.length
                        res.push(temp)
                    }) 
                    resolve(res);
                }else{
                    resolve({})
                }
            }).catch((error)=>{ 
                reject(error);
            })   
        
    })
}

services.getDominanGenre =(user_id)=>{
    
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT genre_title FROM 
        (album INNER JOIN genre ON album.album_id = genre.album_id) 
        WHERE album.user_id = '${user_id}' 
        GROUP BY genre_title 
        ORDER BY COUNT(*) DESC`).then((result)=>{            
            if(result[0]){
                resolve(result[0].genre_title)
            }   
            else{
                resolve("nothing found!!")
            }         
        }).catch((error)=>{
            reject(error) 
        })
        
    })
}
services.getArtistPopularTrack = (user_id)=>{
    
    return new Promise((resolve ,reject)=>{
        PromiseQuery(`SELECT  track.track_id , track.track_title , track.album_id , track.track_duration , track.genre
            FROM album
            INNER JOIN track on track.album_id = album.album_id
            INNER JOIN liked_track ON track.track_id = liked_track.track_id
            INNER JOIN play ON track.track_id = play.track_id
            WHERE album.user_id = '${user_id}'
            GROUP BY track.track_id
            ORDER BY COUNT(track.track_id) DESC 
        `).then((result)=>{
            if(result[0]){
                resolve(result[0])
            }
            else {
                resolve({})
            }     
        }).catch((error)=>{
            reject(error)            
        })
    })
}

services.getArtistsAlbumsWithSameNationality = (id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT *
                    FROM app_user , (SELECT artist.user_id,artist.artistic_name , app_user.nationality 
                                    FROM artist
                                    INNER JOIN app_user ON app_user.user_id = artist.user_id
                                    GROUP BY artist.user_id) AS artists                
                    WHERE app_user.user_id = '${id}' AND app_user.nationality = artists.nationality`).then((result)=>{   
            if(result[0]){                
                let data = []   
                let res = []       
                let checkQuery = '';
                result.forEach((row , index , array)=>{
                    if(index == array.length-1)
                        checkQuery += String(row.user_id)
                    else
                        checkQuery += String(row.user_id) + ','
                })    
                PromiseQuery(`SELECT album_id , album_title , release_date, artist.user_id, artistic_name FROM  
                album INNER JOIN artist ON album.user_id = artist.user_id
                WHERE album.user_id IN (${checkQuery})`).then((res)=>{
                    if(res.length > 0){
                        resolve(res)
                    }
                    else{
                        resolve([])
                    }
                }).catch((error)=>{
                    reject(error)
                })
                // result.forEach((row, index , array)=>{                    
                //     let artist_id = row.user_id;    
                //     console.log(artist_id);                
                //     services.getAlbums(artist_id).then((albums)=>{ 
                //         albums.forEach(item=>{
                //             res.push({
                //                 artist_name : row.artistic_name,
                //                 ...item
                //             })
                //             if(index == array.length-1)
                //             resolve(res)   
                //         })
                          
                //     }).catch((error)=>{
                //         reject(error)
                //     })
                // })
            }
            else{
                resolve([])
            }
            
        }).catch((error)=>{
            reject(error)
        })
    })
}

services.search = (searchQuery)=>{
    return new Promise((resolve , reject)=>{
        let res = {
            users : null,
            tracks : null,
            albums : null,
            playlists : null,
        }
        PromiseQuery(`select user_id , username , firstname , lastname , nationality from app_user
        WHERE (username LIKE '%${searchQuery}%' OR
               firstname LIKE '%${searchQuery}%' OR
               lastname LIKE '%${searchQuery}%' OR
               nationality LIKE '%${searchQuery}%'
        )`).then((users)=>{
            res.users = users            
            PromiseQuery(`SELECT track.track_id , track.track_title , track.genre , track.track_duration, album.album_title , album.album_id , artist.user_id as artsit_id ,artist.artistic_name as artist_name
            FROM track
            INNER JOIN album ON track.album_id =  album.album_id
            INNER JOIN artist ON artist.user_id = album.user_id 
            WHERE (track_title LIKE '%${searchQuery}%' OR
                   genre LIKE '%${searchQuery}%')`).then((tracks)=>{
                       
                res.tracks = tracks;                
                PromiseQuery(`SELECT * FROM album
                INNER JOIN artist ON artist.user_id = album.user_id
                WHERE album_title LIKE '%${searchQuery}%'`).then((albums)=>{                                        
                    res.albums = albums; 
                    PromiseQuery(`SELECT playlist.playlist_id , playlist.playlist_title , app_user.user_id , app_user.username FROM playlist
                    INNER JOIN app_user ON playlist.user_id = app_user.user_id
                    WHERE playlist_title LIKE '%${searchQuery}%'`).then((playlists)=>{
                        res.playlists = playlists;
                        resolve(res)
                    }).catch((error)=>{
                        reject(error)                        
                    })
                }).catch((error)=>{
                    reject(error);
                })
            }).catch((error)=>{
                reject(error);
            })
        }).catch((error)=>{
            reject(error);
        })
  
    })
}


services.getSuspiciousRegularUsers = ()=>{
    return new Promise((resolve , reject)=>{


        //query to check total listening time of regular listener
        PromiseQuery(`SELECT app_user.user_id , app_user.username, SUM(track.track_duration) AS total_listening_time 
                    FROM ((play INNER JOIN track ON play.track_id = track.track_id) INNER JOIN app_user ON play.user_id = app_user.user_id) 
                    WHERE app_user.user_type = "R" GROUP BY app_user.user_id HAVING total_listening_time > 61200`).then((result)=>{                        
                        if(result[0]){
                            let data = result;
                            let suspiciousUsers = []

                            data.forEach((row, index , result)=>{
                                let id = row.user_id
                                let name = row.username
                                services.getFollowers(id).then((followers)=>{
                                    let followersCount = followers.length
                                    if(followersCount == 0){
                                        followersCount = 1
                                    }
                                    services.getFollowing(id).then((following)=>{
                                        let followingsCount = following.length
                                        if( (followingsCount/followersCount) > 5 ){
                                            suspiciousUsers.push({
                                                user_id : id,
                                                user_name : name
                                            })
                                        }
                                        if(index == data.length -1){
                                            resolve(suspiciousUsers);
                                        } 
                                    
                                    }).catch((error)=>{
                                        reject(error)
                                    })
                                }).catch((error)=>{
                                    reject(error)
                                })
                            })
                        }
                        else{
                            resolve([])
                        }
                    
        }).catch((error)=>{
            reject(error)
        })

    }).catch((error)=>{
        reject(error)
    })


}



services.getLastFollowingPlay = (id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT distinct play.user_id , artistic_name ,play.track_id , track.track_title ,  track.genre , track.track_duration , track.album_id , album.album_title , play.last_played_date , play.last_played_time
        FROM follow
        INNER JOIN  play ON follow.following_id = play.user_id
        INNER JOIN track ON track.track_id = play.track_id
        INNER JOIN album ON album.album_id = track.album_id
        INNER JOIN artist ON artist.user_id = album.user_id
        WHERE follow.user_id = '${id}'
        ORDER BY play.user_id DESC,  play.last_played_date DESC, play.last_played_time DESC
        `).then((result)=>{
            if(result[0]){
                let ids = []
                let res = []
                result.forEach(item=>{
                    if(!ids.includes(item.user_id)){
                        res.push(item);
                        ids.push(item.user_id)
                    }
                })
                resolve(res)                
            }
            else 
                resolve([])


        }).catch((error)=>{
            reject(error)
        })
        // PromiseQuery(`SELECT following_id FROM follow 
        // WHERE user_id = '${id}'`).then((result)=>{
        //     if(result[0]){
        //         let user_id ;
        //         let data = [];                
        //         result.forEach((item ,index , array)=>{
        //             user_id = item.following_id; 
        //             PromiseQuery(`SELECT play.track_id ,track.track_title  , track.genre,album.album_id , album.album_title ,play.user_id as following_user_id  FROM play 
        //             INNER JOIN track ON track.track_id = play.track_id
        //             INNER JOIN album ON album.album_id = track.album_id
        //             WHERE play.user_id ='${user_id}' ORDER BY play.last_played_date , play.last_played_time DESC`).then((res)=>{                                        
        //                 if(res[0])
        //                     data.push(res[0])
        //                 if(index ==array.length-1)
        //                     resolve(data)
        //             }).catch((error)=>{ 
        //                 reject(error)
        //             })
                    
        //         })
        //     }
        //     else{
        //         reject("user not have any follower")
        //     }
        // }).catch((error)=>{
        //     reject(error)
            
        // })
    })
}
services.getNewTrackOfArtist = (user_id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT app_user.user_id as artist_id , user_type FROM follow
        INNER JOIN app_user ON app_user.user_id = follow.following_id
        WHERE follow.user_id ='${user_id}' AND user_type='A'`).then((res)=>{
            if(res.length > 0){
                let tracks = []
                res.forEach((row , index , array)=>{
                    PromiseQuery(`SELECT track_id , track_title , track_duration , artist.artistic_name as artist_name , genre , album.album_id , album_title from album
                     INNER JOIN track ON album.album_id = track.album_id
                     INNER JOIN artist ON album.user_id = artist.user_id
                     WHERE album.user_id = '${row.artist_id}'
                     ORDER BY album.release_date DESC
                     LIMIT 5`).then((result)=>{
                        result.forEach(track =>{
                            tracks.push(track)
                        })
                        if(index == array.length-1)
                            resolve(tracks);
                    }).catch((error)=>{
                        reject(error);
                        
                    })
                })
            }
            else{
                resolve([])
            }
          
        }).catch((error)=>{ 
            reject(error)
        })
        // PromiseQuery(`SELECT following_id FROM follow
        // WHERE user_id = '${user_id}'`).then((followings)=>{            
        //     if(followings[0]){
        //         let artists_id = [];
        //         PromiseQuery(`SELECT user_id from app_user WHERE user_type = 'A'`).then((artists)=>{
        //             followings.forEach(flwing=>{
        //                 artists.forEach(artist=>{
        //                     if(flwing.following_id == artist.user_id)
        //                         artists_id.push(flwing.following_id)
        //                 })
        //             })
        //             let tracks = []
                    // artists_id.forEach((id , index , array)=>{
                    //     PromiseQuery(`SELECT track_id , track_title , track_duration , artist.artistic_name as artist_name , genre , album.album_id , album_title from album
                    //     INNER JOIN track ON album.album_id = track.album_id
                    //     INNER JOIN artist ON album.user_id = artist.user_id
                    //     WHERE album.user_id = '${id}'
                    //     ORDER BY album.release_date DESC
                        // LIMIT 5`).then((result)=>{
                            
                        //     result.forEach(track =>{
                        //         tracks.push(track)
                        //     })
                        //     if(index == array.length-1)
                        //         resolve(tracks);
                            
                            
                        // }).catch((error)=>{
                        //     reject(error);
                            
                        // })
        //             })
                                        
                    
        //         }).catch((error)=>{
        //             reject(error);
        //         })
        //     }
        //     else
        //         resolve([])
            
        // }).catch((error)=>{
        //     reject(error);
            
        // })
        
    })
}

services.getArtistsOrderByActivity = ()=>{

    return new Promise((resolve , reject)=>{

        PromiseQuery(`SELECT artist.user_id ,app_user.username , COUNT(track.track_id) AS cnt
        FROM (((track INNER JOIN album ON track.album_id = album.album_id) INNER JOIN artist ON album.user_id = artist.user_id) INNER JOIN app_user ON artist.user_id =app_user.user_id) 
        GROUP BY artist.user_id
        ORDER BY COUNT(track.track_id) DESC`).then((result)=>{

            if(result[0]){
                let artists = []
                result.forEach((row , index , array)=>{
                    let id = row.user_id
                    let name = row.username
                    artists.push({
                        artist_id : id,
                        artist_name : name,
                        tracks_count : row.cnt 
                    })
                    if(index == result.length -1){
                        resolve(artists);
                    } 
                })
            }
            else
                //reject("there is no artists!")\
                resolve([])
            
        }).catch((error)=>{
            reject(error);
            
        })

    })
}

services.getLowActivityArtists = ()=>{

    return new Promise((resolve , reject)=>{

        let date = getDate(0,-100)


        PromiseQuery(`SELECT artist.user_id ,app_user.username , COUNT(track.track_id) AS cnt
        FROM (((track INNER JOIN album ON track.album_id = album.album_id) RIGHT JOIN artist ON album.user_id = artist.user_id) INNER JOIN app_user ON artist.user_id =app_user.user_id) 
        WHERE  (artist.user_id NOT IN (SELECT disapproved_artist.user_id FROM disapproved_artist)) AND ((album.release_date > '${date}') OR (album.release_date IS NULL))
        GROUP BY artist.user_id`).then((result)=>{

            if(result[0]){
                let artists = []
                result.forEach((row , index , array)=>{
                    let id = row.user_id
                    let name = row.username
                    let count = row.cnt
                    if(count<3){
                        artists.push({
                            artist_id : id,
                            artist_name : name,
                            tracks_count : row.cnt 
                        })
                        
                    }
                    if(index == result.length -1){
                        resolve(artists);
                    } 
                })
            }
            else
                //reject("there is no low activity artist!")
                resolve([])
            
        }).catch((error)=>{
            reject(error);
        })

    })
}


services.getFivePopularTrackOfWeek = ()=>{
    return new Promise((resolve , reject)=>{        
        let date = getDate(0 , -7);
        PromiseQuery(`SELECT track_id , COUNT(track_id) FROM play
        WHERE last_played_date > '${date}'
        GROUP BY track_id ORDER BY COUNT(track_id) DESC
        LIMIT 5`).then((played)=>{                        
            let most_played = []
            played.forEach(play=>{
                most_played.push(play.track_id)
            })
            PromiseQuery(`SELECT track_id , COUNT(track_id) FROM liked_track
            WHERE liked_date > '${date}'
            GROUP BY track_id ORDER BY COUNT(track_id) DESC
            LIMIT 5`).then((liked)=>{                                                
                let most_liked = []
                liked.forEach(like=>{
                    most_liked.push(like.track_id)
                })                
                if(most_liked.length > 0 || most_played.length > 0) {
                    let popular = [];
                    var c=most_played.concat(most_liked).sort()
                    var res=c.filter((value,pos) => {return c.indexOf(value) == pos;} );
                    popular = res.splice(0 , 5);
                    let popular_track = []
                    let checkQuery = '';
                    popular.forEach((id , index , array)=>{
                        if(index == array.length-1)
                            checkQuery += String(id)
                        else
                            checkQuery += String(id) + ','
                    })
                    PromiseQuery(`SELECT track.track_id , track.track_title ,artist.artistic_name,track.track_duration , track.genre , track.album_id , album.album_title
                    FROM track
                    INNER JOIN album ON album.album_id = track.album_id
                    INNER JOIN artist ON artist.user_id = album.user_id
                    WHERE track.track_id IN (${checkQuery})`).then((res)=>{
                        if(res.length > 0){
                            resolve(res);
                        }
                        else{
                            resolve([])
                        }
                        
                    }).catch((error)=>{
                        reject(error)
                        
                    })   
                }
                else{
                    resolve([])
                }

            }).catch((error)=>{
                reject(error);
            })
        }).catch((error)=>{ 
            reject(error);
        })
        
    })
}


services.getListenerFavoriteGenre = (id)=>{    
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT track_id FROM play
        WHERE play.user_id = '${id}'
        `).then((playedTrack)=>{
            let ids = []
            
            playedTrack.forEach(item=>{
                ids.push(item.track_id);
            })
            PromiseQuery(`SELECT track_id FROM liked_track
            WHERE user_id = '${id}'`).then((likedTrack)=>{
                likedTrack.forEach(item=>{
                    ids.push(item.track_id);
                })                
                if(ids.length > 0){
                    let mostRepeated = mode(ids);                    
                    PromiseQuery(`SELECT genre 
                    FROM track
                    WHERE track_id='${mostRepeated}'`).then((res)=>{
                        resolve(res[0].genre)
                    }).catch((error)=>{
                        reject(error)
                    })
                }
                else {
                    resolve("nothing found")
                }   
            }).catch((error)=>{
                reject(error)
            })
        }).catch((error)=>{
            reject(error)
        })
    //     PromiseQuery(`SELECT  track.genre 
    //     FROM play INNER JOIN track ON play.track_id=track.track_id
    //     WHERE play.user_id = '${id}' AND (track.genre IS NOT NULL)
    //     GROUP BY track.genre
    //     ORDER BY COUNT(track.genre) DESC
    //     LIMIT 1`).then((fav)=>{            
    //         if(fav[0]){
    //             resolve(fav[0].genre)
    //         }
    //         else{
    //             reject("no favorite genre!")
    //         }
    //     }).catch((error)=>{         
    //         reject(error);
    //     })
        
    })
}

services.getFivePopularTracksOfFavGenre = (fav) =>{    
    return new Promise((resolve ,reject)=>{
        PromiseQuery(`SELECT  track.track_id , track.track_title , track.album_id , album.album_title , track.track_duration , track.genre , artist.artistic_name
        FROM track
        LEFT JOIN liked_track ON track.track_id = liked_track.track_id
        LEFT JOIN play ON track.track_id = play.track_id
        INNER JOIN album ON album.album_id = track.album_id
        INNER JOIN artist ON artist.user_id = album.user_id
        WHERE track.genre = '${fav}'
        GROUP BY track.track_id
        ORDER BY COUNT(track.track_id) DESC 
        LIMIT 5
        `).then((result)=>{            
            if(result[0]){

                let tracks = []
                result.forEach((row , index , array)=>{
                    let tid = row.track_id
                    let title = row.track_title
                    let aid = row.album_id
                    let duration = row.track_duration
                    let genre = row.genre
                    tracks.push({
                        track_id : tid,
                        track_title : title,
                        album_id : aid,
                        track_duration : duration,
                        genre : genre,
                        album_title : row.album_title,
                        artist_name : row.artistic_name
                    })
                    if(index == result.length -1){
                        resolve(tracks);
                    } 
                })

            }
            else {
                resovle([])
            }     
        }).catch((error)=>{
            reject(error)            
        })
    })

}

services.getFiveNewTracksOfFavGenre = (fav) =>{
    

    // chon alan date hamashoon 0e error mide age < koni okaye
    return new Promise((resolve ,reject)=>{
        let date = getDate(0,-7);
        PromiseQuery(`SELECT  track.track_id , track.track_title , track.album_id , album.album_title , track.track_duration , track.genre , album.release_date , artist.artistic_name
        FROM track
        INNER JOIN album ON track.album_id = album.album_id
        INNER JOIN artist ON artist.user_id = album.user_id
        WHERE album.release_date < '${date}' AND track.genre = '${fav}'
        GROUP BY track.track_id
        ORDER BY COUNT(track.track_id) DESC 
        LIMIT 5
        `).then((result)=>{
            if(result[0]){

                let tracks = []
                result.forEach((row , index , array)=>{
                    let tid = row.track_id
                    let title = row.track_title
                    let aid = row.album_id
                    let duration = row.track_duration
                    let genre = row.genre
                    let rd = row.release_date
                    tracks.push({
                        track_id : tid,
                        track_title : title,
                        album_id : aid,
                        track_duration : duration,
                        genre : genre,
                        release_date : rd,
                        album_title : row.album_title,
                        artist_name : row.artistic_name
                    })
                    if(index == result.length -1){
                        resolve(tracks);
                    } 
                })

            }
            else {
                resovle([])
            }     
        }).catch((error)=>{
            reject(error)            
        })
    })
}

services.getdominantGenreOflistenerplayed = (user_id)=>{
    return new Promise((resolve ,reject)=>{
        PromiseQuery(`SELECT track_id ,COUNT(track_id) as played_count
         FROM play 
         WHERE user_id ='${user_id}'
         GROUP BY track_id 
         ORDER BY COUNT(track_id) DESC`).then((result)=>{
            if(result[0]){
                let most_played_id = result[0].track_id;
                PromiseQuery(`SELECT genre FROM track WHERE track_id ='${most_played_id}'`).then((data)=>{
                    if(data[0])
                        resolve(data[0].genre)
                    
                }).catch((error)=>{
                    reject(error)
                })
            }else{
                reject("nothing played yet!");
                
            }
        }).catch((error)=>{
            reject(error); 
        })
        
    })
}



//get following listeners which play at least one music per day after signup
services.getUsersFollowSpeceficUsers = (users)=>{
    //console.log(users)
    let ids = []
    users.forEach((row,index,array)=>{
        ids.push(row.user_id);
    })
    //console.log(ids)

    return new Promise((resolve, reject)=>{
        
        PromiseQuery(`SELECT app_user.user_id , app_user.username
        FROM app_user INNER JOIN follow ON app_user.user_id = follow.user_id
        WHERE app_user.user_id NOT IN (SELECT app_user.user_id 
            FROM app_user 
            INNER JOIN follow ON app_user.user_id = follow.user_id 
            WHERE follow.following_id NOT IN (${ids}) 
            GROUP BY app_user.user_id)
        GROUP BY app_user.user_id`).then((result)=>{
            if(result[0]){
                //console.log(result[0])
                let res = []
                result.forEach((row,index,array)=>{
                    
                    
                    res.push({
                        
                        user_id : row.user_id,
                        username: row.username,
                        firstname: row.firstname,
                        lastname: row.lastname,
                        
                    });

                    if(index == result.length-1){
                        resolve(res);
                    }
                });
            }
            else{
                resolve([])
            }
        }).catch((error)=>{
            reject(error); 
        })

    })
}

services.getUsersListenedAtLeastOneMusicAday =()=>{
    return new Promise((resolve,reject)=>{
         
        let date = getDate(0,0)
        PromiseQuery(`select app_user.user_id , app_user.username , app_user.firstname,app_user.lastname , COUNT(DISTINCT play.last_played_date) as cnt , app_user.signup_date , DATEDIFF('${date}' , app_user.signup_date) AS DateDiff
        from app_user inner join play on app_user.user_id = play.user_id
        GROUP BY app_user.user_id
        HAVING DateDiff <= cnt`).then((result)=>{
            //console.log(result);
            
            if(result[0]){
                let users = []
                result.forEach((row, index, array) => {
                    id = row.user_id
                    users.push({
                        user_id : id,
                        username: row.username,
                        firstname: row.firstname,
                        lastname: row.lastname,
                    })

                    if(index == result.length-1){
                        resolve(users);
                    }
                });
            }
            else{
                //reject("there is no listeners who listened to at least one music per day from his signup!")
                resolve([])
            }
        }).catch((error)=>{
            reject(error); 
        })

    })
}

services.getAllUsers = ()=>{
    
    return new Promise((resolve , reject)=>{
        PromiseQuery(`select user_id , username , firstname , lastname , nationality, user_type, birth_date from app_user`).then((result)=>{
            let res = []
            if(result.length>0) {
                    result.forEach((item, index, array)=>{
                        res.push({
                            user_id : item.user_id,
                            username : item.username,
                            firstname : item.firstname,
                            lastname : item.lastname,
                            nationality: item. nationality,
                            user_type : item.user_type,
                            birth_date: item.birth_date })
                        
                        if(index=array.length-1){
                            resolve(res)
                        } 
                    })
                    } 
        }).catch((error)=>{
            reject(error);
        })
        
    })
}


services.getAllTracks = ()=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`select * from track`).then((result)=>{
            let res = []
            if(result.length>0) {
                result.forEach((item, index, array)=>{
                    res.push({
                        track_id : item.track_id,
                        track_title : item.track_title,
                        genre : item.genre,
                        track_duration : item.track_duration,
                        album_id: item. album_id,
                     })
                    if(index=array.length-1){
                        resolve(res)
                    } 
                })

                }   
        }).catch((error)=>{
            reject(error);
        })
        
    })
}

services.getAllAlbums = ()=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`select * from album`).then((result)=>{
            let res = []
            if(result.length>0) {
                result.forEach((item, index, array)=>{
                    res.push({
                        album_id : item.album_id,
                        album_title : item.album_title,
                        release_date : item.release_date,
                        user_id : item.user_id,
                     })
                    if(index=array.length-1){
                        resolve(res)
                    } 
                })

                }   
        }).catch((error)=>{
            reject(error);
        })
        
    })
}

services.getAllPlaylists = ()=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`select * from playlist`).then((result)=>{
            let res = []
            if(result.length>0) {
                result.forEach((item, index, array)=>{
                    res.push({
                        playlist_id : item.playlist_id,
                        playlist_title : item.playlist_title,
                        user_id : item.user_id
                     })
                    if(index=array.length-1){
                        resolve(res)
                    } 
                })

                }   
        }).catch((error)=>{
            reject(error);
        })
        
    })
}

services.getAllReports = ()=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`select * from report`).then((result)=>{            
            let res = []
            if(result.length>0) {
                result.forEach((item, index, array)=>{
                    res.push({
                        report_message : item.report_message,
                        track_id : item.track_id,
                        user_id : item.user_id
                     })
                    if(index==array.length-1){

                        resolve(res)
                    } 
                })
                }  
                else{
                    resolve([])
                } 
        }).catch((error)=>{
            reject(error);
        })
        
    })
}
// DONE admin get accounts, albums, playlists, reports, tracks
services.adminGetALL = ()=>{
    
    return new Promise((resolve , reject)=>{
        let res = {
            users : [],
            tracks : [],
            albums : [],
            playlists : [],
            reports: []
        }
        services.getAllUsers().then((rUsers)=>{        
                
            res.users=rUsers
            services.getAllTracks().then((rTracks)=>{
                res.tracks=rTracks          
                
                services.getAllAlbums().then((rAlbums)=>{
                    res.albums=rAlbums
                    
                    services.getAllPlaylists().then((rPlaylists)=>{
                        res.playlists=rPlaylists
                        services.getAllReports().then((rReports)=>{
                            res.reports=rReports
                            resolve(res)
                        }).catch((error)=>{
                            reject("coudn't read all reports");
                        })

                    }).catch((error)=>{
                            reject("coudn't read all playlists");
                        })
                }).catch((error)=>{
                    reject("coudn't read all albums");
                })
            }).catch((error)=>{
                reject("coudn't read all tracks");
            })
            }).catch((error)=>{
                reject("coudn't read all users");
            })
        }).catch((error)=>{
            reject("promise failed");
        })
}

services.getDominanGenreOfPlaylist =(playlist_id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT genre , COUNT(genre) FROM playlist_track
        INNER JOIN track ON track.track_id = playlist_track.track_id
        WHERE playlist_id ='${playlist_id}' 
        GROUP BY genre
        ORDER BY COUNT(genre) DESC `)
        .then((res)=>{            
            if(res[0])
                resolve(res[0].genre)
            else    
                resolve("")    
            
        }).catch((error)=>{
            reject(error)
        })
    })
}

services.getArtistFansID = (user_id)=>{
    return new Promise((resolve , reject)=>{
        services.getAlbums(user_id).then((albums)=>{
            let track_ids = []
            albums.forEach(album=>{
                album.tracks.forEach(track=>{
                    track_ids.push(track.track_id)
                })
            })            
            let checkQuery = '';
            track_ids.forEach((id , index , array)=>{
                if(index == array.length-1)
                    checkQuery += String(id)
                else
                    checkQuery += String(id) + ','
            })
            
            
            PromiseQuery(`SELECT user_id , count(*)
            FROM play 
            WHERE track_id IN (${checkQuery})
            GROUP BY user_id
            HAVING count(*) > 10`).then((result)=>{
                if(result[0]){
                    let userslistenToTen = []
                    result.forEach(item=>{
                        userslistenToTen.push(item.user_id)
                    })
                    services.getDominanGenre(user_id).then((artistGenre)=>{
                        let fans = []                        
                        userslistenToTen.forEach((id, index , array)=>{
                            services.getdominantGenreOflistenerplayed(id).then((genre)=>{
                                if(genre == artistGenre)
                                    fans.push(id)
                                if(index == array.length -1)
                                    resolve(fans)    
                            })
                        })
                        
                    }).catch((error)=>{
                        reject(error)
                    })
                    
                }
                else{
                    //reject("no fans found!!")
                    resolve([])
                }
                
            }).catch((error=>{
                reject(error)
                
            }))            
            
        }).catch((error)=>{
            reject(error)

        })
    })
}           
            
services.suggestTwoTracksFromDominantGenre =(genre , playlist_id)=>{
    return new Promise((resolve , reject)=>{
        services.getPlaylistTracks(playlist_id).then((result)=>{
            let playlistTracks = []
            result.forEach(item=>{
                playlistTracks.push(item.track_id)
            })
            let checkQuery = '';
            playlistTracks.forEach((id , index , array)=>{
                if(index == array.length-1)
                    checkQuery += String(id)
                else
                    checkQuery += String(id) + ','
            })  
            PromiseQuery(`SELECT *
        FROM  track
        WHERE (genre = '${genre}' AND track_id NOT IN (${checkQuery}))
            `).then((suggestedTrack)=>{
                if(suggestedTrack.length > 0){
                    if(suggestedTrack.length == 1){
                        PromiseQuery(`SELECT * FROM track
                        INNER JOIN album ON album.album_id = track.album_id
                        INNER JOIN artist ON artist.user_id = album.user_id
                        WHERE track.track_id = '${notInPlaylist[0]}'`).then((res)=>{
                            resolve(res)
                        }).catch((error)=>{
                            reject(error)
                        })
                    }
                    else{
                        const numberOne = uniqueRandom(suggestedTrack.length);
                        const numberTwo = uniqueRandom( suggestedTrack.length, numberOne); 
                        let rand = suggestedTrack[numberOne].track_id + ',' + suggestedTrack[numberTwo].track_id
                        PromiseQuery(`SELECT * FROM track
                        INNER JOIN album ON album.album_id = track.album_id
                        INNER JOIN artist ON artist.user_id = album.user_id
                        WHERE track.track_id IN (${rand})`).then((res)=>{
                            resolve(res)
                            
                        }).catch((error)=>{
                            reject(error)  
                        })
                    }
                } 
                else {
                    resolve([])
                }
                // if(suggestedTrack.length>0){
                //     let notInPlaylist = []   
                //     suggestedTrack.forEach(strack=>{                        
                //         if(!playlistTracks.includes(strack.track_id)) {
                //             notInPlaylist.push(strack.track_id)
                //         }                              
                //     })
                //     if(notInPlaylist.length > 0 ){
                //         if(notInPlaylist.length == 1){
                            // PromiseQuery(`SELECT * FROM track
                            // INNER JOIN album ON album.album_id = track.album_id
                            // INNER JOIN artist ON artist.user_id = album.user_id
                            // WHERE track.track_id = '${notInPlaylist[0]}'`).then((res)=>{
                            //     resolve(res)
                            // }).catch((error)=>{
                            //     reject(error)
                            // })
                //         }
                //         else{
                        //     const numberOne = uniqueRandom(notInPlaylist.length);
                        //     const numberTwo = uniqueRandom( notInPlaylist.length, numberOne); 
                        //     let rand = notInPlaylist[numberOne] + ',' + notInPlaylist[numberTwo]
                        //     PromiseQuery(`SELECT * FROM track
                        //     INNER JOIN album ON album.album_id = track.album_id
                        //     INNER JOIN artist ON artist.user_id = album.user_id
                        //     WHERE track.track_id IN (${rand})`).then((res)=>{
                        //         resolve(res)
                                
                        //     }).catch((error)=>{
                        //         reject(error)
                                
                        //     })
                        // }
                //     }
                //     else {
                //         resolve([])
                //     }
                // } 
                // else{
                //     resolve([])
                // }  
            }).catch((error)=>{
                reject(error) 
            })
                        
        }).catch((error)=>{
            reject(error)
            
        })
    })
}

services.getArtistFans = (user_id)=>{
    return new Promise ((resolve ,reject)=>{
        services.getArtistFansID(user_id).then((data)=>{
            let users = []
            data.forEach((id , index , array)=>{
                getUsername(id).then((name)=>{
                    users.push({
                        user_id : id,
                        name
                    })
                    if(index == array.length-1)
                        resolve(users)
                })
            })
            
        }).catch((error)=>{
            reject(error)
        })
    })
}


services.getFavoriteArtistOfListener = (id)=>{        
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT track_id FROM play
        WHERE play.user_id = '${id}'
        `).then((playedTrack)=>{
            let ids = []
            playedTrack.forEach(item=>{
                ids.push(item.track_id);
            })
            PromiseQuery(`SELECT track_id FROM liked_track
            WHERE user_id = '${id}'`).then((likedTrack)=>{
                likedTrack.forEach(item=>{
                    ids.push(item.track_id);
                })
                if(ids.length > 0){
                    let mostRepeated = mode(ids);
                    PromiseQuery(`SELECT artist.user_id , artistic_name FROM track
                    INNER JOIN album ON album.album_id = track.album_id
                    INNER JOIN artist ON artist.user_id = album.user_id
                    WHERE track_id='${mostRepeated}'`).then((res)=>{
                        resolve(res[0])
                        
                    }).catch((error)=>{
                        reject(error)
                        
                    })
                }
                else {
                    resolve("nothing found")
                }   
            }).catch((error)=>{
                reject(error)
            })
        }).catch((error)=>{
            reject(error)
        })
        // PromiseQuery(`SELECT * FROM play LEFT JOIN liked_track ON play.track_id = liked_track.track_id
        // UNION
        // SELECT * FROM play RIGHT JOIN liked_track ON play.track_id = liked_track.track_id
        // `).then((res)=>{
            
        // }).catch((error)=>{
            
        // })
        // PromiseQuery(`SELECT album.user_id , app_user.username
        // FROM track
        // INNER JOIN liked_track ON track.track_id = liked_track.track_id
        // INNER JOIN album ON track.album_id = album.album_id
        // INNER JOIN app_user ON album.user_id = app_user.user_id
        // WHERE liked_track.user_id = '${id}'
        // `).then((artist)=>{
        //     console.log(artist);
            
        //     if(artist[0]){
        //         let userid = artist[0].user_id
        //         let name = artist[0].username
        //         resolve({
        //             user_id : userid, 
        //             username : name
        //         })
        //     }
        //     else{
        //         resolve([])
        //     }
        // }).catch((error)=>{
        //     reject(error)
        // })

    })
}

services.getArtistWithSpecificDomGenre = (id,genre)=>{
    
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT *
        From artist
        INNER JOIN (SELECT genre_title, album.user_id
                FROM album INNER JOIN genre ON album.album_id = genre.album_id
                GROUP BY genre_title , album.user_id ) AS domGen ON domGen.user_id = artist.user_id
        INNER JOIN app_user ON artist.user_id = app_user.user_id        
        WHERE artist.user_id <> '${id}' AND domGen.genre_title = '${genre}'
        GROUP BY artist.user_id`).then((res)=>{
            if(res[0]){                                
                let artists = []
                res.forEach((row , index , array)=>{
                    
                    let userid = row.user_id
                    artists.push({
                        user_id: userid,
                        artistic_name: row.artistic_name,
                        username : row.username,    
                        firstname : row.firstname,
                        lastname: row.lastname,
                        nationality : row.nationality
                    })
                    if(index == res.length-1)
                        resolve(artists)
                })
            }
            else{
                resolve([])
            }
        }).catch((error)=>{
            reject(error)
        })
    })
}
//-----------------------------------all below on delete cascades
//DONE delete account
services.deleteAccount = (data)=>{
    return new Promise ((resolve  , reject)=>{
        PromiseQuery(`SELECT user_type FROM app_user WHERE (user_id = '${data.user_id}')`).then((resUserType)=>{
            //user TYPES A artist, P premium, R regular
            PromiseQuery(`DELETE FROM app_user WHERE (user_id = '${data.user_id}')`).then(()=>{
                // if(resUserType=='R') {
                //     resolve("Regular account successfully deleted!")
                // }
                // else if(resUserType=='P') {
                //     PromiseQuery(`DELETE FROM premium_listener WHERE (user_id = '${data.user_id}')`).then(()=>{
                //             resolve("premium account successfully deleted!")
                //         }).catch((error)=>{
                //             reject("failure in regular account deletion!");
                //         })
                //     }
                // else if(resUserType=='A') {
                //     PromiseQuery(`DELETE FROM artist WHERE (user_id = '${data.user_id}')`).then(()=>{
                //             resolve("artist account successfully deleted!")
                //         }).catch((error)=>{
                //             reject("failure in artist account deletion!");
                //         })
                //     }
                resolve();
            }).catch((error)=>{
                reject("failure in regular account deletion!");
            })
            
            }).catch((error)=>{
                reject("User account not found!");
            })
    })
}

// DONE deletePlaylist
services.adminDeletePlaylist = (data)=>{
    return new Promise ((resolve  , reject)=>{
        PromiseQuery(`DELETE  FROM playlist WHERE (playlist_id = '${data.playlist_id}')`).then(()=>{
                resolve("playlist succesfully deleted")
            }).catch((error)=>{
                reject("playlist not found!");
            })
    })
}

//DONE deleteAlbum
services.adminDeleteAlbum = (data)=>{
    return new Promise ((resolve  , reject)=>{
        PromiseQuery(`DELETE  FROM album WHERE (album_id = '${data.album_id}')`).then(()=>{
                resolve("album succesfully deleted")
            }).catch((error)=>{
                reject("album not found!");
            })
    })
}

//DONE deleteTrack
services.adminDeleteTrack = (data)=>{
    return new Promise ((resolve  , reject)=>{
        PromiseQuery(`DELETE  FROM track WHERE (track_id = '${data.track_id}')`).then(()=>{
                resolve("track succesfully deleted")
            }).catch((error)=>{
                reject("track not found!");
            })
    })
}

//DONE deleteReport
//API in case needed
services.adminDeleteReport = (data)=>{
    return new Promise ((resolve  , reject)=>{
        PromiseQuery(`DELETE  FROM report WHERE (report_id = '${data.report_id}')`).then(()=>{
                resolve("report succesfully deleted")
            }).catch((error)=>{
                reject("report not found!");
            })
    })
}


services.editPLaylistName = (data)=>{

    return new Promise((resolve,reject)=>{
        PromiseQuery(`SELECT playlist.playlist_id
        FROM playlist 
        WHERE playlist_title = '${data.playlist_title}' AND user_id = '${data.user_id}' AND NOT EXISTS (SELECT * 
                                                                                FROM liked_playlist INNER JOIN playlist ON playlist.playlist_id = liked_playlist.playlist_id
                                                                                WHERE playlist_title = '${data.playlist_title}' AND liked_playlist.user_id = '${data.user_id}')`).then((res)=>{
            if(res[0]){
                let pid = res[0].playlist_id
                pool.query(`UPDATE playlist 
                SET playlist_title = '${data.new_title}' 
                WHERE playlist.user_id = '${data.user_id}' AND playlist.playlist_id = '${pid}' AND playlist.playlist_title = '${data.playlist_title}'` , (error , result)=>{
                    if(error)
                        reject(error)
                    else{
                        resolve() 
                    }
                })

            }
            else{
                reject("cant edit this playlist or playlist with this information doesnt exist!")
            }

        }).catch((error)=>{
            reject(error)
        })
    })

}


services.listenerDeletePlaylist = (data)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`DELETE FROM playlist WHERE (playlist_id = '${data.playlist_id}' AND user_id = '${data.user_id}')`).then((res)=>{
            if(res.affectedRows > 0){
                PromiseQuery(`DELETE FROM liked_playlist WHERE (playlist_id = '${data.playlist_id}' AND user_id = '${data.user_id}')`).then(()=>{
                    resolve()
                })
                
            }
            else
                reject("playlist not found")     
        }).catch((error)=>{
            reject(error)
        })
    })
}

services.getLikedTracks = (user_id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT * from liked_track WHERE user_id = '${user_id}'`).then((res)=>{
            resolve(res)
            
        }).catch((error)=>{
            reject(error);
            
        })
        
    })
}

services.getLikedPlaylist = (user_id)=>{
    return new Promise((resolve  ,reject)=>{
        PromiseQuery(`SELECT playlist_id from liked_playlist WHERE (user_id ='${user_id}')`).then((result)=>{
            resolve(result)
        }).catch((error)=>{
            reject(error)
        })
    })
}

services.getPlaylistTracks = (playlist_id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT * FROM playlist_track WHERE playlist_id = '${playlist_id}'`).then((res)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    })
}

services.test = (q)=>{
    PromiseQuery(`SELECT *  FROM album
    INNER JOIN artist ON artist.user_id = album.user_id
    WHERE album_title LIKE '%${q}%'`).then((albums)=>{
        console.log(albums);
        
    })
        
    
    
}


module.exports = services;

const validPass = (enteredPass , existPass , salt)=>{
    let hash = createPass(enteredPass , salt);
    return hash === existPass
}

const createPass = (enteredPass , salt)=>{
    return crypto.pbkdf2Sync(enteredPass, salt,1000, 64, `sha512`).toString("hex");
}
const createSalt = ()=>{
    return crypto.randomBytes(16).toString('hex')
}

const getUserID = (username , email)=>{
    return new Promise((resolve , reject)=>{
        pool.query(`SELECT user_id FROM app_user WHERE (username = "${username}" OR email = "${email}")` , (error , result)=>{
            if(error){
                reject(error)
            }
    
            if(result[0])
                resolve(result[0].user_id)    
            else
                reject(error)
        })

    })       
}
const getUsername = (id)=>{
    return new Promise((resolve , reject)=>{
        pool.query(`SELECT username FROM app_user WHERE user_id ='${id}'` , (error , result)=>{
            if(error){
                reject(error)
            }
            if(result[0])
                resolve(result[0].username)    
            else
                reject(error)
        })

    }) 
}



const getAlbumID = (album_title , user_id)=>{    
    return new Promise((resolve , reject)=>{
        pool.query(`SELECT album_id FROM album WHERE (album_title = '${album_title}' AND user_id = '${user_id}')`, (error , result)=>{
            
            if(error)
                reject(error)
            if(result[0])
                resolve(result[0].album_id)    
            else
                reject("album not found!!")   
                      
        })          
    })
}




const isUserNotPremium = (id)=>{
    return new Promise((resolve , reject)=>{
        PromiseQuery(`SELECT user_type FROM app_user WHERE user_id = '${id}'`).then((result)=>{                            
            if(result[0].user_type === "R")
                resolve()
            else if(result[0].user_type=== "P")
                reject("user is already is premium!");            
            else 
                reject("user not found!!")
        }).catch((err)=>{
            return reject(err);
        })
    })
       
    }

const PromiseQuery = (query)=>{
    return new Promise((resolve, reject)=>{
        pool.query(query , (error , result)=>{
            if(error)
                reject(error)
            else 
                resolve(result)    
        })
    })
}

const getDate = (month=0 , day = 0)=>{
    var date = new Date();
    date.setMonth(date.getMonth() + month)
    date.setDate(date.getDate() + day);
    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    date = yyyy + '-' + mm + '-' + dd;
    return date;
}
function mode(array)
{
    if(array.length == 0)
        return null;
    var modeMap = {};
    var maxEl = array[0], maxCount = 1;
    for(var i = 0; i < array.length; i++)
    {
        var el = array[i];
        if(modeMap[el] == null)
            modeMap[el] = 1;
        else
            modeMap[el]++;  
        if(modeMap[el] > maxCount)
        {
            maxEl = el;
            maxCount = modeMap[el];
        }
    }
    return maxEl;
}
const uniqueRandom = (renge , ...compareNumbers) => {
    let uniqueNumber;
    do {
        uniqueNumber = Math.floor(Math.random() * renge);
    } while(compareNumbers.includes(uniqueNumber));
    return uniqueNumber;
};
