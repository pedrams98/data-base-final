-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2020 at 09:04 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spotify_demo_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `album_id` int(11) NOT NULL,
  `album_title` varchar(50) NOT NULL,
  `release_date` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `album_title`, `release_date`, `user_id`) VALUES
(68, 'album1-artist1', '2020-02-03', 168),
(71, 'album2-artist1', '2020-02-03', 168),
(72, 'album1-artist2', '2020-02-03', 169),
(73, 'album2-artist2', '2020-02-03', 169),
(74, 'album1-artist3', '2020-02-03', 170),
(75, 'album2-artist3', '2020-02-03', 170),
(76, 'album1-artist4', '2020-02-03', 172),
(77, 'album2-artist4', '2020-02-03', 172),
(78, 'album2-artist5', '2020-02-03', 171),
(79, 'album1-artist6', '2020-02-03', 175);

-- --------------------------------------------------------

--
-- Table structure for table `app_user`
--

CREATE TABLE `app_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `passwordHash` varchar(200) NOT NULL,
  `salt` varchar(200) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `user_type` char(1) NOT NULL,
  `birth_date` date NOT NULL,
  `signup_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `app_user`
--

INSERT INTO `app_user` (`user_id`, `username`, `email`, `passwordHash`, `salt`, `firstname`, `lastname`, `nationality`, `user_type`, `birth_date`, `signup_date`) VALUES
(158, 'listener1', 'listener1', 'c18f2d3bf0e4502be9d9883843ad95f678569a44628db33e445132c498a8574dff01fdbb3888f95025d5b4e14df9e4e410bc14b95d2d1344e0128796cda55b74', '7c818d20444cc61a04128bbcf7e76372', 'listener1', 'listener1', 'IR', 'R', '2020-05-06', '2020-07-14'),
(159, 'listener2', 'listener2', 'ed20c16a2601b45976618d7c84da754e17b5670afc28b52b18b8c0747b6e2920a2764bcc25410ac906a446e87ccdf6470ffc8db9ec3e3e724a5ab7664b1e7723', '8ad3300baddb730088f31b0f73de706c', 'listener2', 'listener2', 'AR', 'R', '2020-05-06', '2020-07-14'),
(160, 'listener3', 'listener3', '61cb6f22c87d4287095864c45636b4bb8d1ac81f8ae445cba24c8f119c668477052a6f9e158beea4ea7c4c52e30ae243c97c82fac446f4754b53f90b6070d654', 'd7a897b0beaea5f9c68ac4d2dbedb478', 'listener3', 'listener3', 'IR', 'R', '2020-05-06', '2020-07-14'),
(161, 'listener4', 'listener4', '29bb2defd09fef230aa3c6523caacc25d54a3f949fc18a3b9c57981b0fe4a5dbfbf503786e6b7433242777b98b5e6839dd90030ce0feb7aeecc56a883267acb5', 'ab463e54ffb56b655516a325551e9f4a', 'listener4', 'listener4', 'US', 'P', '2020-05-06', '2020-07-14'),
(162, 'listener5', 'listener5', '4956656e1e763d5ea268e7ed800a1d391271c1898fc322cc1640b1a222b6dbd9ccbf242918ca50131cc6d26bad33b46853ee03f72908b0a195299d2900c3b511', 'b5cd89d52baa227400ecf8a9a070714f', 'listener5', 'listener5', 'US', 'R', '2020-05-06', '2020-07-14'),
(163, 'listener6', 'listener6', '2fc4678beb4f7f1e6dc1c7b15d860a7c6efef74792fd3240974f272bea6dfd2ed807094ed1b2c4f0b0559a7bb553549220bdb1b815bcae1dfe2e4a1bb9afe017', '5e943956b72c1142ea9ef7ec39819113', 'listener6', 'listener6', 'US', 'R', '2020-05-06', '2020-07-14'),
(164, 'listener7', 'listener7', '32cc0453399fb34d60e5e1166b9eb020ce908ad936717a0ff47d0eef51fbf88b090b89c5dd7d673158c43ade790b6c697c0548ba09f6268362347ead551dc6ba', 'd450867607284750afe4afb93859bd5b', 'listener7', 'listener7', 'IR', 'R', '2020-05-06', '2020-07-14'),
(165, 'listener8', 'listener8', 'df21b2b4cb129bd23eabde55c32fdc63487fab5cc1a0855c72cb4f0b9a49f774df6cd81692f865084662e21edb49568aea5c3491ece4056506df101b8504c65b', 'ad740f019afaa8a9bed1296b2e4a80da', 'listener8', 'listener8', 'IR', 'R', '2020-05-06', '2020-07-14'),
(166, 'listener9', 'listener9', '32ea6eda4c6818874163ee8fde358934ae7ab362e2daa01d2d39ec361dccc7c254eaf928a3532166fe6811c8d686a6a55821132518b44734f07197f8157e52cc', '5600e9eb411c9504b765acfb7096d052', 'listener9', 'listener9', 'IR', 'R', '2020-05-06', '2020-07-14'),
(167, 'listener10', 'listener10', '8568c778d415c06f9734aafe6519a48617e154a68c3bca70b7b8cecc0f84d9a7f37b30eaed6429d2fbd4104747950dfa414dc5a368b4b056ae194637958cd2f7', '6f110746aa3a4e0c1841386d4b633556', 'listener10', 'listener9', 'AR', 'A', '2020-05-06', '2020-07-14'),
(168, 'artist1', 'artist1', '4d18013dbad43095319387ba21c17535b5b86782c30146e59f5a5a8049e1bba79c6d2dbaf9e5d8e673098abe52eaba29fe1375147d9a6305da60649ec5f418d4', 'aee3a10370ec151d6854c3e622dbcdfd', 'undefined', 'artist1', 'IR', 'A', '2020-05-31', '2020-07-14'),
(169, 'artist2', 'artist2', 'be1405cafca637387cc1ab02765b4e2b68086ad9e93e7497d539957e8d05cc0ca85b6ecb817db235478c40cbbcfb29740e684ec56a04c22ef4be97ef14edd9d2', '54241dac28e06535680dcfc9e3407308', 'undefined', 'artist2', 'IR', 'A', '2020-05-31', '2020-07-14'),
(170, 'artist3', 'artist3', 'f50f8eaae5685041f71a74e81d6615a2aacbcb5b448a3f28766c9da2190a8af0679ece6989b60a003639b64e76e118191350dfb3601c3c0c993a2835f8769e1a', 'a4ba62e46a051572aeca2b42dd40e821', 'undefined', 'artist3', 'US', 'A', '2020-05-31', '2020-07-14'),
(171, 'artist5', 'artist5', '06524aadfa65e6f9c27c28b5642818cf06214ec178e632a81ad76dcad802ae57c3b2fcc6545834ee2e568cb03597759d1cfb3c813bd3867a5ae81ddd8d538570', '76707e19b473bf2e1a0d7d225892abe6', 'undefined', 'artist5', 'US', 'A', '2020-05-31', '2020-07-14'),
(172, 'artist4', 'artist4', 'd1a8287b5ef1a2091d62e28298f02acdd081e28226904f6da35f79e2799d1c5424197d7049fd17e11ad90cf2afeb376b116061fd72626b050235e069f7458b60', '54647b3d06c833f668467215cdbce73a', 'undefined', 'artist4', 'AR', 'A', '2020-05-31', '2020-07-14'),
(175, 'artist6', 'artist6', '3a0072556bc791db97eacc4328c8a93e8c1fba876794149e60f0fe6e838556babf883ef37593e386d5802d63b23f480fb276bcd032b6f201a613eb04669db864', '3345c8b27cb55c2594cb517f946b1cc2', 'artist6', 'artist6', 'artist6', 'A', '2020-05-31', '2020-07-16'),
(176, 'pedram', 'pedram@gmail.com', '38c17ff87e0dd9b67a36a5ffcbbfc6856a5648e6675631c0bdae1194bcc4c5903033155b3085b3421a53ae4e1b5f5ad2501bf26f5272356cc83d38ed597eaea4', '8a2dae0eb3da72b4434ebc7e88c13759', 'pedram', 'soofi', 'IR', 'R', '1998-02-01', '2020-07-16');

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `artistic_name` varchar(20) NOT NULL,
  `activity_start_data` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`artistic_name`, `activity_start_data`, `user_id`) VALUES
('artist1', '2020-05-31', 168),
('artist2', '2020-05-31', 169),
('artist3', '2020-05-31', 170),
('artist5', '2020-05-31', 171),
('artist4', '2020-05-31', 172),
('artist6', '2020-05-31', 175);

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE `credit_card` (
  `card_number` bigint(32) NOT NULL,
  `expiration_date` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `credit_card`
--

INSERT INTO `credit_card` (`card_number`, `expiration_date`, `user_id`) VALUES
(1234432112334566, '2023-05-01', 161);

-- --------------------------------------------------------

--
-- Table structure for table `disapproved_artist`
--

CREATE TABLE `disapproved_artist` (
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `user_id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`user_id`, `following_id`) VALUES
(158, 159),
(158, 160),
(158, 161),
(158, 166),
(158, 168),
(158, 169),
(158, 170),
(158, 171),
(158, 172),
(159, 158),
(159, 159),
(159, 167),
(159, 168),
(159, 169),
(159, 172),
(160, 159),
(160, 167),
(160, 168),
(160, 169),
(160, 170),
(160, 171),
(160, 175),
(161, 159),
(161, 167),
(161, 168),
(161, 169),
(161, 170),
(161, 171),
(161, 172),
(161, 175),
(162, 159),
(162, 160),
(162, 161),
(162, 163),
(162, 164),
(162, 165),
(162, 166),
(162, 167),
(162, 168),
(162, 169),
(162, 171),
(164, 158),
(164, 159),
(164, 160),
(164, 161),
(164, 162),
(164, 163),
(164, 165),
(164, 166),
(164, 168),
(164, 169),
(164, 170),
(164, 171),
(165, 158),
(165, 159),
(165, 160),
(165, 161),
(165, 162),
(165, 163),
(165, 167),
(165, 169),
(166, 158),
(166, 159),
(166, 160),
(166, 161),
(166, 163),
(166, 167),
(166, 168),
(166, 169);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `genre_title` varchar(50) NOT NULL,
  `album_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`genre_title`, `album_id`) VALUES
('genre1', 71),
('genre1', 73),
('genre1', 75),
('genre1', 77),
('genre1', 78),
('genre2', 68),
('genre2', 72),
('genre2', 74),
('genre2', 76),
('genre2', 79),
('genre3', 68),
('genre3', 72),
('genre3', 74),
('genre3', 76),
('genre3', 79);

-- --------------------------------------------------------

--
-- Table structure for table `liked_playlist`
--

CREATE TABLE `liked_playlist` (
  `user_id` int(11) NOT NULL,
  `playlist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `liked_playlist`
--

INSERT INTO `liked_playlist` (`user_id`, `playlist_id`) VALUES
(159, 82),
(159, 83),
(160, 90),
(160, 91),
(161, 90),
(161, 91),
(162, 91),
(164, 80),
(164, 83),
(165, 91),
(166, 90),
(166, 91);

-- --------------------------------------------------------

--
-- Table structure for table `liked_track`
--

CREATE TABLE `liked_track` (
  `track_id` int(11) NOT NULL,
  `liked_date` date NOT NULL,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `liked_track`
--

INSERT INTO `liked_track` (`track_id`, `liked_date`, `playlist_id`, `user_id`) VALUES
(144, '2020-07-16', 82, 160),
(144, '2020-07-16', 83, 161),
(144, '2020-07-16', 84, 162),
(144, '2020-07-16', 86, 164),
(144, '2020-07-16', 87, 165),
(144, '2020-07-16', 88, 166),
(145, '2020-07-16', 82, 160),
(145, '2020-07-16', 83, 161),
(145, '2020-07-16', 84, 162),
(145, '2020-07-16', 86, 164),
(145, '2020-07-16', 87, 165),
(145, '2020-07-16', 88, 166),
(146, '2020-07-16', 82, 160),
(146, '2020-07-16', 83, 161),
(146, '2020-07-16', 84, 162),
(146, '2020-07-16', 86, 164),
(146, '2020-07-16', 87, 165),
(146, '2020-07-16', 88, 166),
(147, '2020-07-16', 83, 161),
(147, '2020-07-16', 84, 162),
(147, '2020-07-16', 86, 164),
(147, '2020-07-16', 87, 165),
(147, '2020-07-16', 88, 166),
(148, '2020-07-16', 82, 160),
(148, '2020-07-16', 83, 161),
(148, '2020-07-16', 84, 162),
(148, '2020-07-16', 86, 164),
(148, '2020-07-16', 88, 166),
(150, '2020-07-16', 80, 158),
(150, '2020-07-16', 83, 161),
(153, '2020-07-16', 84, 162),
(154, '2020-07-16', 83, 161),
(154, '2020-07-16', 88, 166),
(155, '2020-07-16', 83, 161),
(155, '2020-07-16', 86, 164),
(156, '2020-07-16', 83, 161),
(157, '2020-07-16', 82, 160),
(157, '2020-07-16', 88, 166),
(158, '2020-07-16', 82, 160),
(159, '2020-07-16', 84, 162),
(159, '2020-07-16', 87, 165),
(160, '2020-07-16', 84, 162),
(160, '2020-07-16', 87, 165),
(161, '2020-07-16', 84, 162),
(163, '2020-07-16', 84, 162),
(164, '2020-07-16', 84, 162),
(165, '2020-07-16', 84, 162),
(167, '2020-07-16', 83, 161),
(168, '2020-07-16', 84, 162),
(175, '2020-07-16', 84, 162),
(181, '2020-07-16', 84, 162);

-- --------------------------------------------------------

--
-- Table structure for table `play`
--

CREATE TABLE `play` (
  `track_id` int(11) NOT NULL,
  `last_played_date` date NOT NULL,
  `last_played_time` time NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `play`
--

INSERT INTO `play` (`track_id`, `last_played_date`, `last_played_time`, `user_id`) VALUES
(144, '2020-07-14', '16:26:14', 158),
(144, '2020-07-14', '16:34:40', 159),
(144, '2020-07-16', '11:12:57', 161),
(144, '2020-07-16', '12:15:56', 166),
(145, '2020-07-14', '16:26:16', 158),
(145, '2020-07-14', '16:34:41', 159),
(145, '2020-07-16', '11:12:58', 161),
(145, '2020-07-16', '11:14:03', 161),
(145, '2020-07-16', '11:25:41', 162),
(145, '2020-07-16', '12:15:55', 166),
(145, '2020-07-16', '12:18:08', 166),
(146, '2020-07-14', '16:26:17', 158),
(146, '2020-07-14', '16:34:42', 159),
(146, '2020-07-16', '11:12:58', 161),
(146, '2020-07-16', '11:25:40', 162),
(146, '2020-07-16', '12:15:54', 166),
(147, '2020-07-14', '16:26:19', 158),
(147, '2020-07-16', '11:25:39', 162),
(147, '2020-07-16', '12:15:58', 166),
(148, '2020-07-14', '16:26:20', 158),
(148, '2020-07-14', '17:10:53', 159),
(148, '2020-07-16', '11:12:59', 161),
(149, '2020-07-16', '11:25:43', 162),
(150, '2020-07-16', '11:25:43', 162),
(154, '2020-07-16', '11:04:49', 158),
(154, '2020-07-14', '17:10:54', 159),
(157, '2020-07-16', '11:10:28', 160),
(157, '2020-07-16', '11:10:36', 160),
(157, '2020-07-16', '11:22:27', 161),
(157, '2020-07-16', '11:35:39', 164),
(158, '2020-07-16', '11:10:29', 160),
(158, '2020-07-16', '11:22:25', 161),
(158, '2020-07-16', '11:35:38', 164),
(158, '2020-07-16', '00:00:00', 165),
(159, '2020-07-16', '11:10:30', 160),
(159, '2020-07-16', '11:22:25', 161),
(159, '2020-07-16', '11:35:39', 164),
(159, '2020-07-16', '12:08:58', 165),
(159, '2020-07-16', '12:09:03', 165),
(160, '2020-07-16', '11:10:33', 160),
(160, '2020-07-16', '11:22:23', 161),
(160, '2020-07-16', '11:35:41', 164),
(160, '2020-07-16', '12:08:53', 165),
(160, '2020-07-16', '12:08:58', 165),
(161, '2020-07-16', '11:22:28', 161),
(161, '2020-07-16', '11:35:42', 164),
(162, '2020-07-16', '11:22:29', 161),
(163, '2020-07-16', '11:22:30', 161),
(165, '2020-07-16', '00:00:00', 161),
(165, '2020-07-16', '11:22:32', 161),
(166, '2020-07-16', '11:22:35', 161),
(167, '2020-07-16', '11:14:01', 161);

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `playlist_id` int(11) NOT NULL,
  `playlist_title` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`playlist_id`, `playlist_title`, `user_id`) VALUES
(80, 'liked', 158),
(82, 'liked', 160),
(83, 'liked', 161),
(84, 'liked', 162),
(85, 'liked', 163),
(86, 'liked', 164),
(87, 'liked', 165),
(88, 'liked', 166),
(89, 'liked', 167),
(112, 'liked', 176),
(80, 'likedlistener1', 164),
(82, 'likedlistener3', 159),
(83, 'likedlistener4', 159),
(83, 'likedlistener4', 164),
(83, 'likedShared', 170),
(90, 'listener1-playlist1', 158),
(90, 'listener1-playlist1', 159),
(90, 'listener1-playlist1', 160),
(90, 'listener1-playlist1', 161),
(90, 'listener1-playlist1', 166),
(90, 'listener1-playlist1', 167),
(91, 'listener1-playlist2', 158),
(91, 'listener1-playlist2', 160),
(91, 'listener1-playlist2', 161),
(91, 'listener1-playlist2', 162),
(91, 'listener1-playlist2', 165),
(91, 'listener1-playlist2', 166),
(91, 'listener1-playlist2', 169),
(92, 'listener1-playlist3', 158),
(93, 'listener2-playlist1', 159),
(94, 'listener2-playlist2', 159),
(96, 'listener3-playlist1', 160),
(97, 'listener3-playlist2', 160),
(98, 'listener4-playlist1', 161),
(99, 'listener4-playlist2', 161),
(100, 'listener4-playlist3', 161),
(101, 'listener4-playlist4', 161),
(102, 'listener6-playlist1', 162),
(103, 'listener6-playlist2', 162),
(104, 'listener6-playlist3', 162),
(105, 'listener7-playlist1', 164),
(106, 'listener7-playlist2', 164),
(107, 'listener8-playlist1', 165),
(108, 'listener8-playlist2', 165),
(109, 'listener8-playlist4', 165),
(110, 'listener9-playlist1', 166),
(111, 'listener9-playlist2', 166);

-- --------------------------------------------------------

--
-- Table structure for table `playlist_track`
--

CREATE TABLE `playlist_track` (
  `track_id` int(11) NOT NULL,
  `playlist_id` int(11) NOT NULL,
  `added_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playlist_track`
--

INSERT INTO `playlist_track` (`track_id`, `playlist_id`, `added_date`) VALUES
(144, 82, '2020-07-16'),
(144, 83, '2020-07-16'),
(144, 84, '2020-07-16'),
(144, 86, '2020-07-16'),
(144, 87, '2020-07-16'),
(144, 88, '2020-07-16'),
(144, 90, '2020-07-14'),
(144, 96, '2020-07-16'),
(144, 100, '2020-07-16'),
(144, 103, '2020-07-16'),
(144, 104, '2020-07-16'),
(144, 105, '2020-07-16'),
(144, 108, '2020-07-16'),
(144, 110, '2020-07-16'),
(145, 82, '2020-07-16'),
(145, 83, '2020-07-16'),
(145, 84, '2020-07-16'),
(145, 86, '2020-07-16'),
(145, 87, '2020-07-16'),
(145, 88, '2020-07-16'),
(145, 90, '2020-07-14'),
(145, 100, '2020-07-16'),
(145, 102, '2020-07-16'),
(145, 106, '2020-07-16'),
(145, 108, '2020-07-16'),
(146, 82, '2020-07-16'),
(146, 83, '2020-07-16'),
(146, 84, '2020-07-16'),
(146, 86, '2020-07-16'),
(146, 87, '2020-07-16'),
(146, 88, '2020-07-16'),
(146, 96, '2020-07-16'),
(146, 97, '2020-07-16'),
(146, 106, '2020-07-16'),
(146, 110, '2020-07-16'),
(147, 83, '2020-07-16'),
(147, 84, '2020-07-16'),
(147, 86, '2020-07-16'),
(147, 87, '2020-07-16'),
(147, 88, '2020-07-16'),
(147, 106, '2020-07-16'),
(147, 107, '2020-07-16'),
(147, 110, '2020-07-16'),
(148, 82, '2020-07-16'),
(148, 83, '2020-07-16'),
(148, 84, '2020-07-16'),
(148, 86, '2020-07-16'),
(148, 88, '2020-07-16'),
(148, 93, '2020-07-14'),
(148, 98, '2020-07-16'),
(148, 102, '2020-07-16'),
(149, 92, '2020-07-14'),
(149, 100, '2020-07-16'),
(149, 106, '2020-07-16'),
(150, 80, '2020-07-16'),
(150, 83, '2020-07-16'),
(150, 90, '2020-07-14'),
(150, 94, '2020-07-14'),
(150, 96, '2020-07-16'),
(151, 91, '2020-07-14'),
(152, 105, '2020-07-16'),
(153, 84, '2020-07-16'),
(153, 102, '2020-07-16'),
(154, 83, '2020-07-16'),
(154, 88, '2020-07-16'),
(154, 93, '2020-07-14'),
(154, 96, '2020-07-16'),
(155, 83, '2020-07-16'),
(155, 86, '2020-07-16'),
(155, 93, '2020-07-14'),
(155, 105, '2020-07-16'),
(156, 83, '2020-07-16'),
(156, 102, '2020-07-16'),
(157, 82, '2020-07-16'),
(157, 88, '2020-07-16'),
(158, 82, '2020-07-16'),
(158, 94, '2020-07-14'),
(158, 97, '2020-07-16'),
(159, 84, '2020-07-16'),
(159, 87, '2020-07-16'),
(160, 84, '2020-07-16'),
(160, 87, '2020-07-16'),
(160, 102, '2020-07-16'),
(161, 84, '2020-07-16'),
(161, 91, '2020-07-14'),
(162, 100, '2020-07-16'),
(163, 84, '2020-07-16'),
(164, 84, '2020-07-16'),
(165, 84, '2020-07-16'),
(165, 100, '2020-07-16'),
(167, 83, '2020-07-16'),
(168, 84, '2020-07-16'),
(175, 84, '2020-07-16'),
(181, 84, '2020-07-16'),
(181, 102, '2020-07-16');

-- --------------------------------------------------------

--
-- Table structure for table `premium_listener`
--

CREATE TABLE `premium_listener` (
  `membership_from` date NOT NULL,
  `membership_till` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `premium_listener`
--

INSERT INTO `premium_listener` (`membership_from`, `membership_till`, `user_id`) VALUES
('2020-07-16', '2025-04-16', 161);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `report_message` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `track_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`report_message`, `user_id`, `track_id`) VALUES
('report1\n\n', 161, 167);

-- --------------------------------------------------------

--
-- Table structure for table `security_answers`
--

CREATE TABLE `security_answers` (
  `ans1` varchar(100) NOT NULL,
  `ans2` varchar(100) NOT NULL,
  `ans3` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `security_answers`
--

INSERT INTO `security_answers` (`ans1`, `ans2`, `ans3`, `user_id`) VALUES
('listener1', 'listener1', 'listener1', 158),
('listener2', 'listener2', 'listener2', 159),
('listener3', 'listener3', 'listener3', 160),
('listener4', 'listener4', 'listener4', 161),
('listener5', 'listener5', 'listener5', 162),
('listener6', 'listener6', 'listener6', 163),
('listener7', 'listener7', 'listener7', 164),
('listener8', 'listener8', 'listener8', 165),
('listener9', 'listener9', 'listener9', 166),
('listener10', 'listener10', 'listener10', 167),
('artist1', 'artist1', 'artist1', 168),
('artist2', 'artist2', 'artist2', 169),
('artist3', 'artist3', 'artist3', 170),
('artist5', 'artist5', 'artist5', 171),
('artist4', 'artist4', 'artist4', 172),
('artist6', 'artist6', 'artist6', 175),
('test', 'test', 'test', 176);

-- --------------------------------------------------------

--
-- Table structure for table `track`
--

CREATE TABLE `track` (
  `track_id` int(11) NOT NULL,
  `track_title` varchar(50) NOT NULL,
  `genre` varchar(32) NOT NULL,
  `track_duration` float NOT NULL,
  `album_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `track`
--

INSERT INTO `track` (`track_id`, `track_title`, `genre`, `track_duration`, `album_id`) VALUES
(144, 'al1-ar1-tr1', 'genre3', 120, 68),
(145, 'al1-ar1-tr2', 'genre2', 120, 68),
(146, 'al1-ar1-tr5', 'genre2', 120, 68),
(147, 'al1-ar1-tr4', 'genre2', 120, 68),
(148, 'al1-ar1-tr7', 'genre2', 120, 68),
(149, 'al1-ar1-tr6', 'genre2', 120, 68),
(150, 'al1-ar1-tr3', 'genre2', 120, 68),
(151, 'al1-ar1-tr8', 'genre2', 120, 68),
(152, 'album2-artist1', 'genre1', 120, 71),
(153, 'al1-ar2-tr1', 'genre3', 120, 72),
(154, 'al1-ar2-tr3', 'genre2', 120, 72),
(155, 'al1-ar2-tr2', 'genre2', 120, 72),
(156, 'al1-ar2-tr4', 'genre2', 120, 72),
(157, 'al1-ar2-tr6', 'genre2', 120, 72),
(158, 'al1-ar2-tr7', 'genre2', 120, 72),
(159, 'al1-ar2-tr8', 'genre2', 120, 72),
(160, 'al1-ar2-tr5', 'genre2', 120, 72),
(161, 'album2-artist2', 'genre1', 120, 73),
(162, 'al1-ar3-tr8', 'genre2', 120, 74),
(163, 'al1-ar3-tr7', 'genre2', 120, 74),
(164, 'al1-ar3-tr1', 'genre3', 120, 74),
(165, 'al1-ar3-tr6', 'genre2', 120, 74),
(166, 'al1-ar3-tr5', 'genre2', 120, 74),
(167, 'al1-ar3-tr2', 'genre2', 120, 74),
(168, 'al1-ar3-tr3', 'genre2', 120, 74),
(169, 'al1-ar3-tr4', 'genre2', 120, 74),
(170, 'album2-artist3', 'genre1', 120, 75),
(171, 'al1-ar4-tr8', 'genre2', 120, 76),
(172, 'al1-ar4-tr7', 'genre2', 120, 76),
(173, 'al1-ar4-tr6', 'genre2', 120, 76),
(174, 'al1-ar4-tr5', 'genre2', 120, 76),
(175, 'al1-ar4-tr1', 'genre3', 120, 76),
(176, 'al1-ar4-tr4', 'genre2', 120, 76),
(177, 'al1-ar4-tr2', 'genre2', 120, 76),
(178, 'al1-ar4-tr3', 'genre2', 120, 76),
(179, 'album2-artist4', 'genre1', 120, 77),
(180, 'album2-artist5', 'genre1', 120, 78),
(181, 'al1-ar6-tr1', 'genre3', 120, 79),
(182, 'al1-ar6-tr2', 'genre2', 120, 79),
(183, 'al1-ar6-tr3', 'genre2', 120, 79),
(184, 'al1-ar6-tr4', 'genre2', 120, 79),
(185, 'al1-ar6-tr5', 'genre2', 120, 79),
(186, 'al1-ar6-tr6', 'genre2', 120, 79),
(187, 'al1-ar6-tr7', 'genre2', 120, 79),
(188, 'al1-ar6-tr8', 'genre2', 120, 79);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`,`user_id`),
  ADD UNIQUE KEY `album_title` (`album_title`),
  ADD KEY `fk_album_user_id` (`user_id`);

--
-- Indexes for table `app_user`
--
ALTER TABLE `app_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `artistic_name` (`artistic_name`);

--
-- Indexes for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`user_id`,`card_number`);

--
-- Indexes for table `disapproved_artist`
--
ALTER TABLE `disapproved_artist`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`user_id`,`following_id`),
  ADD KEY `fk_follow_following_id` (`following_id`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`genre_title`,`album_id`),
  ADD KEY `fk_genre_album_id` (`album_id`);

--
-- Indexes for table `liked_playlist`
--
ALTER TABLE `liked_playlist`
  ADD PRIMARY KEY (`user_id`,`playlist_id`),
  ADD KEY `fk_liked_playlist_playlist_id` (`playlist_id`);

--
-- Indexes for table `liked_track`
--
ALTER TABLE `liked_track`
  ADD PRIMARY KEY (`track_id`,`user_id`,`playlist_id`),
  ADD KEY `fk_liked_track_playlist_id` (`playlist_id`),
  ADD KEY `fk_liked_track_user_id` (`user_id`);

--
-- Indexes for table `play`
--
ALTER TABLE `play`
  ADD PRIMARY KEY (`track_id`,`user_id`,`last_played_date`,`last_played_time`) USING BTREE,
  ADD KEY `fk_play_user_id` (`user_id`);

--
-- Indexes for table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`playlist_id`,`user_id`),
  ADD KEY `fk_playlist_user_id` (`user_id`),
  ADD KEY `playlist_title` (`playlist_title`) USING BTREE;

--
-- Indexes for table `playlist_track`
--
ALTER TABLE `playlist_track`
  ADD PRIMARY KEY (`track_id`,`playlist_id`),
  ADD KEY `fk_playlist_track_playlist_id` (`playlist_id`);

--
-- Indexes for table `premium_listener`
--
ALTER TABLE `premium_listener`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`user_id`,`track_id`),
  ADD KEY `fk_report_track_id` (`track_id`);

--
-- Indexes for table `security_answers`
--
ALTER TABLE `security_answers`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `track`
--
ALTER TABLE `track`
  ADD PRIMARY KEY (`track_id`),
  ADD UNIQUE KEY `track_title` (`track_title`),
  ADD KEY `fk_track_album_id` (`album_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `app_user`
--
ALTER TABLE `app_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `playlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `track`
--
ALTER TABLE `track`
  MODIFY `track_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `fk_album_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `artist`
--
ALTER TABLE `artist`
  ADD CONSTRAINT `fk_artist_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD CONSTRAINT `fk_credit_card_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `disapproved_artist`
--
ALTER TABLE `disapproved_artist`
  ADD CONSTRAINT `fk_disspproved_artist_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `follow`
--
ALTER TABLE `follow`
  ADD CONSTRAINT `fk_follow_following_id` FOREIGN KEY (`following_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_follow_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `genre`
--
ALTER TABLE `genre`
  ADD CONSTRAINT `fk_genre_album_id` FOREIGN KEY (`album_id`) REFERENCES `album` (`album_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `liked_playlist`
--
ALTER TABLE `liked_playlist`
  ADD CONSTRAINT `fk_liked_playlist__user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_liked_playlist_playlist_id` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`playlist_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `liked_track`
--
ALTER TABLE `liked_track`
  ADD CONSTRAINT `fk_liked_track_playlist_id` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`playlist_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_liked_track_track_id` FOREIGN KEY (`track_id`) REFERENCES `track` (`track_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_liked_track_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `play`
--
ALTER TABLE `play`
  ADD CONSTRAINT `fk_play_track_id` FOREIGN KEY (`track_id`) REFERENCES `track` (`track_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_play_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `playlist`
--
ALTER TABLE `playlist`
  ADD CONSTRAINT `fk_playlist_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `playlist_track`
--
ALTER TABLE `playlist_track`
  ADD CONSTRAINT `fk_playlist_track_playlist_id` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`playlist_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_playlist_track_track_id` FOREIGN KEY (`track_id`) REFERENCES `track` (`track_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `premium_listener`
--
ALTER TABLE `premium_listener`
  ADD CONSTRAINT `fk_premium_listener_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `fk_report_track_id` FOREIGN KEY (`track_id`) REFERENCES `track` (`track_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_report_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `security_answers`
--
ALTER TABLE `security_answers`
  ADD CONSTRAINT `fk_security_answers_user_id` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `track`
--
ALTER TABLE `track`
  ADD CONSTRAINT `fk_track_album_id` FOREIGN KEY (`album_id`) REFERENCES `album` (`album_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
